\chapter[Tratamiento num\'{e}rico de procesos radiativos en choques
internos de flujos re\-la\-ti\-vis\-tas magnetizados][Tratamiento
num\'{e}rico de procesos radiativos en flujos relativistas]{Tratamiento
  num\'{e}rico de procesos radiativos en choques internos de flujos
  relativistas magnetizados}
\label{cha:resumen}

Los bl\'{a}zares son un tipo de n\'{u}cleo activo de galaxia (\ac{agn}, por
sus siglas en ing\'{e}s) que se encuentran entre los objetos
astrof\'{\i}sicos m\'{a}s energ\'{e}ticos y violentos, a la par de los
brotes de rayos $\gamma$ (\ac{grb} por sus siglas en ingl\'{e}s). Los
procesos f\'{\i}sicos y, en particular, el escenario del chorro relativista
en el que se genera la radiaci\'{o}n ultraenerg\'{e}tica detectada por
observatorios terrestres y en \'{o}rbita, han conseguido atraer la
atenci\'{o}n e inter\'{e}s de los astr\'{o}nomos y astrof\'{\i}sicos desde
su descubrimiento. En la presente tesis investigamos el modelo de choques
internos (\ac{is}) cuya hip\'{o}tesis consta del choque de dos capas de
plasma con geometr\'{\i}a cil\'{\i}ndrica, formando dos ondas de choque que
atraviesan las antedichas capas acelerando electrones a su paso: tanto
t\'{e}rmicos como no t\'{e}rmicos. Dichos electrones interaccionan con el
campo magn\'{e}tico presente en el chorro produciendo, de acuerdo con las
observaciones, emisi\'{o}n \mbs~(\ac{mbs}). En este modelo consideramos
tambi\'{e}n que el chorro se encuentra envuelto en un ambiente de fotones
monocrom\'{a}tico, que equivaldr\'{\i}a a la regi\'{o}n de banda ancha
(\ac{blr}) de un \ac{agn}. Ambos tipos de fotones, los del medio externo y
los producidos in situ, eventualmente interaccionan con los electrones
acelerados mediante la dispersi\'{o}n Compton inversa (\ac{ic}).

El objetivo b\'{a}sico de la presente tesis ha sido la b\'{u}squeda de
alg\'{u}n indicio que pudiera revelar las huellas dejadas tanto por la
magnetizaci\'{o}n de las capas como por la distribuci\'{o}n energ\'{e}tica
de los electrones (\ac{eed}) inyectados en el frente de choque, en la
distribuci\'{o}n espectral de energ\'{\i}a (\ac{sed}). Nuestro enfoque ha
sido num\'{e}rico, lo cual significa que se desarrollaron herramientas
num\'{e}ricas sofisticadas que hemos usado sistem\'{a}ticamente para
simular el modelo de \ac{is} y reproducir las \acp{sed} espectralmente
amplias de los blazares. Datos observacionales fueron empleados para
corroborar dichas simulaciones y delimitar el espacio de parametros para,
de esta forma, conseguir que nuestras \acp{sed} sint\'{e}ticas estuviesen
en concordancia con las observaciones.

Mostramos a partir de simulaciones que, si examinamos la dominancia
Compton, $A_C$, y el \'{\i}ndice espectral de fotones, $\gph$, en la banda
de rayos $\gamma$, una parte considerable de la \emph{sequencia de los
  bl\'{a}zares} podr\'{\i}a ser explicada por la magnetizaci\'{o}n de las
capas; siendo las menos magnetizadas las que se encuentran en la regi\'{o}n
de los radiocu\'{a}sares de espectro plano (\acp{fsrq}), mientras que las
capas medianamente magnetizadas caen en la regi\'{o}n de los objetos
\emph{BL Lacertae} (\ac{bllac}). Por otra parte, al incluir electrones
t\'{e}rmicos en la poblaci\'{o}n inyectada, y agregar una herramienta
num\'{e}rica que nos permite reproducir la emisi\'{o}n \ac{mbs} de
electrones poco energ\'{e}ticos, encontramos que el valle que separa las
componentes sincrotr\'on e \ac{ic}, se hace m\'{a}s ``profundo'' cuando las
distribuciones inyectadas en el frente de choque son dominadas por
electrones t\'{e}rmicos. Para estos casos descubrimos que el pico
sincrotr\'on var\'{\i}a ligeramente entre modelos (entre
\SIrange{1e11}{1e13}{\hertz}), al contrario de una componente \ac{ic}
sensible a la variaci\'{o}n de par\'{a}metros. Este efecto induce una
disperci\'{o}n vertical en el plano dominancia Compton-pico sincrotr\'{o}n,
sugiriendo que quiz\'{a} la proporci\'{o}n de electrones t\'{e}rmicos sobre
los no t\'{ermicos} est\'{a} relacionada con la posici\'{o}n de los
bl\'{a}zares en ese plano.


\section{Prefacio}
\label{sec:prefacio}

En el Cap\'{\i}tulo~\ref{cha:theor-backgr} profundizamos en los conceptos
f\'{\i}sicos sobre los que reside la emisi\'{o}n \ac{mbs} y el modelo de
\ac{is} para blazars. Empezamos describiendo la din\'{a}mica y
electrodin\'{a}mica de una part\'{\i}cula cargada inmersa en un campo
magn\'{e}tico homog\'{e}neo. Continuamos con la descripci\'{o}n de la
din\'{a}mica de de la colisi\'{o}n de dos capas de plasma, como parte del
modelo de \ac{is}, bas\'{a}ndonos en el trabajo de
\citet{Mimica:2012aa}. La \'{u}ltima parte de dicho cap\'{\i}tulo consiste
en la descripci\'{o}n de los diferentes tipos de distribuciones de
part\'{\i}culas, c\'{o}mo modelamos la inyecci\'{o}n en un frente de onda
de choque y la evoluci\'{o}n de part\'{\i}culas que se encuentran en una
regi\'{o}n que ha sido afectada por una onda de choque, considerando tanto
el caso en el que no existe un t\'{e}rmino de enfriamiento en la
ecuaci\'{o}n de evoluci\'{o}n como el caso en el que s\'{\i} se considera
dicho t\'{e}rmino en un intervalo de tiempo finito.

En el Cap\'{\i}tulo~\ref{cha:blazars-code} se engloba la descripci\'{o}n de
las t\'{e}cnicas num\'{e}ricas y m\'{e}todos desarrollados y usados durante
mi estancia en el grupo de investigaci\'{o}n \ac{camap} con el Prof.~Miguel
A. Aloy y el Dr.~Petar Mimica. La primera parte de dicho cap\'{\i}tulo
est\'{a} centrada en el c\'{o}digo para \ac{is} desarrollado por
\citet{Mimica:2012aa}, el cual es la piedra angular de la presente
tesis. En la segunda parte describo detalladamente el c\'{o}digo \chamba:
una nueva herramienta computacional cuya intenci\'{o}n es la de reproducir
la emisividad \ac{mbs} producida por una part\'{\i}cula cargada y por una
distribuci\'{o}n de part\'{\i}culas con perfil arbitrario. Nuestras
publicaciones \citep{Rueda:2014mn,Rueda:2017hd} corresponden a resultados
obtenidos del uso sistem\'{a}tico estas herramientas.

El Cap\'{\i}tulo~\ref{cha:prmint} est\'{a} basado en la investigaci\'{o}n
hecha en el grupo \ac{camap} con el Prof.~Miguel A. Aloy y el Dr.~Petar
Mimica, como continuaci\'{o}n del trabajo previamente publicado por los
miembros de este grupo \citep{Mimica:2012aa}. Nuestro trabajo fue sujeto a
revisi\'{o}n por pares y publicado en 2014:
\bibentry{Rueda:2014mn}. \'{E}ste consisti\'{o} en ampliar la
exploraci\'{o}n del espacio de par\'{a}metros del c\'{o}digo para \acp{is}
desarrollado por el Dr.~Petar Mimica y el Prof.~Miguel A. Aloy para simular
el modelo de \ac{is} para blazars, incluyendo el c\'{o}mputo del
\'{\i}ndice espectral de fotones a partir de nuestras simulaciones
(descrito en \Sref{sec:photon-index}). Los datos observacionales que se
muestran en dicho trabajo fueron obtenidos de la base de datos
correspondiente al segundo cat\'{a}logo de n\'{u}cleos activos de galaxias
del instrumento LAT a bordo del sat\'{e}lite \emph{Fermi} (\ac{2lac}, por
sus siglas en ingl\'{e}s)%
\footnote{\url{https://heasarc.gsfc.nasa.gov/W3Browse/all/fermilac.html}}.
Todas las simulaciones para este trabajo fueron realizadas en el
superordenador \texttt{Tirant} de la Universidad de Valencia.

El Cap\'{\i}tulo~\ref{cha:hybdis} est\'{a} basado en la investigaci\'{o}n
hecha en el grupo \ac{camap} con el Prof.~Miguel A. Aloy y el
Dr.~Petar. Nuestro trabajo se someti\'{o} a revisi\'{o}n por pares y
result\'{o} en un art\'{\i}culo publicado en 2017:
\bibentry{Rueda:2017hd}. Los resultados ah\'{\i} descritos son derivados de
simulaciones hechas usando el c\'{o}digo de \ac{is} \citep{Mimica:2012aa}
para distribuciones h\'{\i}bridas de part\'{\i}culas (t\'{e}rmicas y no
t\'{e}rmicas). Adem\'{a}s de la implementaci\'{o}n de la nueva herramienta
num\'{e}rica \chamba~(\Sref{sec:chamba})

La astrof\'{\i}sica ha sido para mi como salir a la aventura por la
naturaleza. En ning\'{u}n momento supe con qu\'{e} me iba a topar en el
camino pero cada paso me ha tra\'{i}do alegr\'{\i}a y nuevos
aprendizajes. Saber un poco de los fen\'{o}menos que ocurren all\'{a}
arriba ha sido como respirar el aire fresco de los bosques, cuya brisa
purifica los pulmones y te llena de paz, o como una lluvia torrencial que
te empapa de ese vital l\'{\i}quido pero que es tanto que tienes que correr
en busca de refugio. El proceso de proponer el desarrollo de una nueva
herramienta num\'{e}rica como \chamba~me ha ayudado a tener una idea de lo
que hay fuera. El proceso de escritura de dicho c\'{o}digo, por otro lado,
ha sido como subir a una monta\~{n}a: nunca sabes si conseguir\'{a}s llegar
a la cima o si la naturaleza terminar\'{a} enviando una tormenta que te
obligar\'{a} a replegar. Nuestro ascenso empez\'{o} en el campamento base
\emph{electrones ultra energ\'{e}ticos}. All\'{\i} planeamos nuestra ruta,
siempre al pendiente de cualquier previsi\'{o}n de tempestad. Durante
nuestros d\'{\i}as en el campamento base tuvimos que respondernos a
cuestiones fundamentales como si realmente vale la pena ir m\'{a}s all\'{a}
de las cumbres transrelativistas, a las que muchos exploradores han subido
y bajado con gran destreza por d\'{e}cadas, o quedarnos en las faldas del
monte \ac{mbs}. Con un impulso blazar\'{\i}stico decidimos abandonar la
tranquilidad del campamento base adentrarnos en el coraz\'{o}n de la
monta\~{n}a. En el camino tuvimos que atravesar barrancos de grava
(num\'{e}rica) inestable, escalar y rapelar despe\~{n}aderos y riscos
impresionantes. Ergios y ergios de paredes verticales. Hubo momentos,
incluso, en los que no hab\'{\i}a un solo agarre a la vista, rezando por
que la cuerda aguantara. Para fortuna de nuestra expedici\'{o}n lo hizo, y
conseguimos llegar a la cima ciclotr\'{o}n.



%% Translation of the Motivation. Sec. 1.5, p.~16
%% >>>>>>>>>>>>>>>>>>>>>>>>>>>
\section{Objetivos}
\label{sec:objetivos}

En un intento por entender las observaciones de blazares, se han propuesto
muchos modelos e hip\'{o}tesis a lo largo de los a\~{n}os que han abierto
un abanico de ideas sobre la f\'{\i}sica involucrada en los procesos de
altas energ\'{\i}as que en ellos ocurren. A\'{u}n as\'{\i}, no hay
suficiente evidencia observacional que nos indique el grado de importancia
de cada proceso f\'{\i}sico que interviene en la producci\'{o}n de la
emisi\'{o}n ultraenerg\'{e}tica observada (p.~ej., es bien sabido que los
campos magn\'{e}ticos juegan un papel importante en los flujos
relativistas, pero no conocemos la magnetizaci\'{o}n del chorro ni sabemos
con certeza si los campos magn\'{e}ticos juegan un papel importante en los
procesos de disipaci\'{o}n los chorros). Hay modelos que se han propuesto
para clasificar y unificar los \acp{agn} (y blazars en particular). Sin
embargo, no sabemos con certeza si estos modelos describen la verdadera
naturaleza de los \acp{agn}.

Vivimos en una \'{e}poca en la que los observatorios existentes y
venideros, tanto terrestres como espaciales, observan el universo en muchas
bandas espectrales, de modo que se espera que en los pr\'{o}ximos a\~{n}os
nueva informaci\'{o}n salga a la luz (valga la redundancia
electromagn\'{e}tica) de todo tipo de objetos astrof\'{\i}sicos; con los
bl\'{a}zares entre ellos. Sin embargo, la imposibilidad de replicar en el
laboratorio las condiciones necesarias para observar y medir los procesos
que producen, p. ej., las fulguraciones en blazares ha favorecido el
desarrollo constante de c\'{o}digos num\'{e}ricos sofisticados que ejecutan
simulaciones de estos procesos. Dichas simulaciones nos ayudan a tener una
intuici\'{o}n f\'{\i}sica de los fen\'{o}menos astrof\'{\i}sicos; ya sea
por comparaci\'{o}n con los datos observacionales existentes, o por la
predicci\'{o}n de las propiedades de futuros eventos. Los c\'{o}digos de
\'{u}ltima generaci\'{o}n para bl\'{a}zares incorporan tanta macro y
microf\'{\i}sica como las capacidades computacionales lo permitan.
T\'{\i}picamente, existe un equilibrio entre ambas. Es decir, uno necesita
decidir cu\'{a}nto esfuerzo se dedica a las escalas macrosc\'{o}picas
(p. ej., procesos \ac{mhd}) y cu\'{a}nto a las microsc\'{o}picas (p. ej.,
la aceleraci\'{o}n en una onda de choque). En nuestro trabajo
constantemente tratamos de mejorar nuestro modelado a ambos rangos de las
escalas din\'{a}micas en bl\'{a}zares.

El modelo de \ac{is} para bl\'{a}zares ha logrado modelar datos
observacionales \citep[e.g.][]{Bottcher:2010gn}. %
%% <<<<<<<<<<<<<<<<<<<<<<<<<<<
%% Translation of RMA17, p. 1863
%% >>>>>>>>>>>>>>>>>>>>>>>>>>>
Uno de nuestros principales objetivos de la presente tesis es el de ampliar
el estudio del espacio de par\'{a}metros, iniciados por
\citetalias{Mimica:2012aa}, para el modelo de \ac{is} mediante el
c\'{a}lculo de la emisi\'{o}n, dependiente del tiempo y para m\'{u}ltiples
longitudes de onda, de varias familias de modelos; principalmente
caracterizados por la magnetizaci\'{o}n de las capas. %
%% <<<<<<<<<<<<<<<<<<<<<<<<<<<
%% Translation Motivation
%% >>>>>>>>>>>>>>>>>>>>>>>>>>>
Partiendo de trabajos previos
\citep{Bottcher:2010gn,Mimica:2004phd,Mimica:2012aa,Mahadevan:1996ek,Leung:2011aj},
pretendemos profundizar en la exploraci\'{o}n del modelo de \ac{is} con el
plan de identificar en las \acp{sed} y curvas de luz, las huellas dejadas
por la magnetizaci\'{o}n de las capas (macro escalas) y por las distintas
propiedades de las part\'{\i}culas inyectados en el frente de onda de
choque (micro escalas). Tomamos en cuenta la existencia de part\'{\i}culas
subrelativistas y transrelativistas en la poblaci\'{o}n de part\'{\i}culas
inyectadas, por lo que englobamos los procesos de emisi\'{o}n
ciclotr\'{o}n, sincrotr\'{o}n y ciclo-sincrotr\'{o}n.
%% <<<<<<<<<<<<<<<<<<<<<<<<<<<



\section{Metodolog\'{\i}a}
\label{sec:metodologia}

%% Translation of RMA17, p. 1169
%% >>>>>>>>>>>>>>>>>>>>>>>>>>>
En la presente tesis se estudian los mecanismos de emisi\'{o}n en blazars,
una subclase de \ac{agn} en la que un chorro relativista se propaga en
direcci\'{o}n cercana a la l\'{\i}nea de visi\'{o}n de un observador en la
Tierra \citep{Urry:1995aa}. Una componente importante de la radiaci\'{o}n
observada en blazars es producida por la emisi\'{o}n no t\'{e}rmica de
dicho chorro. Su espectro muestra dos crestas muy anchas. La primera
est\'{a} situada entre radiofrecuencias y rayos X, mientras que la segunda
aparece entre los rayos X y los $\gamma$ \citep[p.~ej.][]{Fossati:1998ay}.\
Dependiendo de las frecuencias de m\'{a}xima luminosidad y la intensidad de
las l\'{\i}neas de emisi\'{o}n, los blazars pueden subdividirse en objetos
\ac{bllac} y \ac{fsrq} \citep{Giommi:2012aa}. Existe un consenso general
de que el pico que aparece a bajas frecuencias se debe a la emisi\'{o}n
sincrotr\'{o}n de electrones relativistas que giran en un campo
magn\'{e}tico. En cuanto que al pico a altas frecuencias, actualmente hay
dos modelos en disputa%
\begin{enumerate*}[label={(\alph*)},before=\unskip{: },itemjoin={{, }},itemjoin*={{, y }}]
\item el modelo lept\'{o}nico que propone que la la emisi\'{o}n a altas
  energ\'{\i}as es producida por los electrones relativistas que dispersan
  mediante el proceso \ac{ic} tanto a los fotones fr\'{\i}os del medio
  externo (Compton inverso externo; \ac{eic}) y a los fotones tipo
  sincrotr\'{o}n producidos in situ en el chorro (auto-Compton de
  sincrotr\'{o}n; \ac{ssc})
\item el modelo hadr\'{o}nico que plantea la existencia de protones
  relativistas en el chorro que, en presencia de campos magn\'{e}ticos muy
  intensos, son capaces de producir radiaci\'{o}n ultraenerg\'{e}tica a
  trav\'{e}s tanto del proceso sincrotr\'{o}n directamente, como de
  cascadas electromagn\'{e}ticas
\end{enumerate*}
\citep[v\'{e}ase][para una discusi\'{o}n detallada de ambos
modelos]{Bottcher:2010wp}. En la presente tesis limitamos nuestro estudio
al modelo lept\'{o}nico.
%%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

%% Translation of RMA14 p.~1856
%% >>>>>>>>>>>>>>>>>>>>>>>>>>>
En este trabajo, nos concentramos exclusivamente en la contribuci\'{o}n del
chorro relativista. El escenario de \ac{is}
\citep[p.~ej.][]{Rees:1994ca,Spada:2001do,Mimica:2004ay} ha sido exitoso en
explicar muchas de las caracter\'{\i}sticas sobre la variabilidad en
bl\'{a}zares. Como hip\'{o}tesis central se encuentra la idea de que la
presencia de movimientos relativos en el chorro relativista eventualmente
producir\'{a} choques de capas densas de plasma fr\'{\i}o. En el transcurso
de la colisi\'{o}n de dichas capas, el plasma sufre los efectos del choque
y parte de la energ\'{\i}a cin\'{e}tica del chorro es disipada en el
relativamente d\'{e}bil \ac{is}, lo que explicar\'{\i}a las fulguraciones
observadas en las curvas de luz de estos eventos. En las dos \'{u}ltimas
d\'{e}cadas este escenario ha sido explorado a fondo utilizando modelos
anal\'{\i}ticos y simplificados
\citep{Kobayashi:1997vf,Daigne:1998wq,Bosnjak:2009dv,Daigne:2011aa}
as\'{\i} como tambi\'{e}n por medio de simulaciones de hidrodin\'{a}mica
num\'{e}rica \citep{Kino:2004in,Mimica:2004ay,Mimica:2007aa}.

Esta tesis contin\'{u}a a lo largo de las l\'{\i}neas esbozadas en trabajos
anteriores \citep[MA12 de aqu\'{\i} en adelante]{Mimica:2012aa}, y extiende
los trabajos publicados en el periodo de doctorado \citep[RMA14 y RMA17 de
aqu\'{\i} en adelante,
respectivamente]{Rueda:2014mn,Rueda:2017hd}. \citetalias{Mimica:2012aa}
extiende el trabajo sobre la disipaci\'{o}n (eficiencia din\'{a}mica) de
los \acp{is} magnetizados \citep{Mimica:2010aa}, incluyendo procesos
radiativos de una manera similar a los modelos detallados recientes para el
c\'{a}lculo de la emisi\'{o}n de los \acp{is}
\citep{Bottcher:2010gn,Joshi:2011bp,Chen:2011fo}. En
\citetalias{Mimica:2012aa} se asume que la luminosidad del flujo es
constante, pero se var\'{\i}a el grado de magnetizaci\'{o}n de las capas
para investigar las consecuencias de dicha variaci\'{o}n en los espectros y
curvas de luz observadas. Encuentran que la eficiencia radiativa de una
sola colisi\'{o}n de capas es m\'{a}xima cuando una de ellas est\'{a}
altamente magnetizada y la otra posee un campo magn\'{e}tico d\'{e}bil o
casi nulo. De igual menera, los autores proponen una manera de distinguir
observacionalmente entre colisiones de capas escasa y altamente
magnetizadas por medio de la comparaci\'{o}n entre las frecuencias
m\'{a}ximas y fluencias de las componentes sinctrotr\'{o}n y \ac{ic}.

Una de las limitaciones del estudio mostrado por \citetalias{Mimica:2012aa}
es que s\'{o}lo se var\'{\i}a la magnetizaci\'{o}n de las capas (aunque
cuberiendo una gama relativamente amplia del potencial espacio de
par\'{a}metros), dejando el resto de par\'{a}metros sin cambios. En este
trabajo se presentan los resultados de un estudio param\'{e}trico m\'{a}s
sistem\'{a}tico en el que se consideran tres combinaciones de
magnetizaciones de las capas que \citet{Mimica:2012aa} consideraron de
inter\'{e}s, pero variando tanto los par\'{a}metros cinem\'{a}ticos (el
factor de Lorentz del fluido y la velocidad relativa de las capas) como los
par\'{a}metros extr\'{\i}nsecos (p.~ej., el \'{a}ngulo de visi\'{o}n
$\theta$ del chorro), mientras que los par\'{a}metros
microf\'{\i}sicos se fijan a valores t\'{\i}picos.
%%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

%% Translation of RMA17, p. 1169
%% >>>>>>>>>>>>>>>>>>>>>>>>>>>
La radiaci\'{o}n emitida por blazes resulta de la disipaci\'{o}n del flujo
cin\'{e}tico y del flujo tipo Poynting. En la presente tesis consideramos
el modelo de \ac{is}, en el cual la disipaci\'{o}n antes mencionada es
producida por la colisi\'{o}n de capas densas de plasma fr\'{\i}o dentro
del chorro relativista
\citep[p.~ej.][]{Rees:1994ca,Spada:2001do,Mimica:2004ay}. Cada colisi\'{o}n
de dichas capas puede producir \acp{is} que aceleran a los electrones que
son, a fin de cuentas, los responsables de la emisi\'{o}n observada.

Nos centramos tambi\'{e}n en estudiar la influencia de las propiedades de
la \ac{eed} en la emisi\'{o}n observada. \citet{Giannios:2009zm}
propusieron una \ac{eed}; es decir Maxwelliana m\'{a}s no t\'{e}rmica
(denominada en lo subsiguiente como ``distribuci\'{o}n h\'{\i}brida'', o
simplemente \ac{hd}) como una explicaci\'{o}n de algunas de las
caracter\'{\i}sticas de la emisi\'{o}n temprana y de la posluminiscencia en
\acp{grb}. Para llevar a cabo dicho objetivo, introducimos una \ac{hd} en
nuestro c\'{o}digo num\'{e}rico y estudiamos c\'{o}mo afecta a las curvas
de luz y espectros de emici\'{o}n en bl\'{a}zares.

No obstante, dado que la componente t\'{e}rmica en una \ac{hd} la
energ\'{\i}a de los electrones se extiende hasta reg\'{\i}menes
subrelativistas, necesitamos replantear el mecanismo de emisi\'{o}n
(sincrotr\'{o}n) empleado hasta ahora. La radiaci\'{o}n de part\'{\i}culas
cargadas que atraviesan un campo magn\'{e}tico se conoce como
\ac{mbs}. Dependiendo de la velocidad $\beta c$ de las part\'{\i}culas,
esta radiaci\'{o}n se clasifica en ciclotr\'{o}n si $\beta \sim 1$, y
sincrotr\'{o}n si $\beta \sim 1$. Ambos reg\'{\i}menes han sido estudiados
ampliamente y se han desarrollado expresiones anal\'{\i}ticas precisas para
cada uno
\citep[p.~ej.][]{Ginzburg:1965gc,Pacholczyk:1970,Rybicki:1979}. Sin
embargo, la radiaci\'{o}n ciclo-sincrotr\'{o}n, es decir, el r\'{e}gimen
transrelativista, no cuenta con una descripci\'{o}n anal\'{\i}tica
sencilla. Cumpliendo con uno de los objetivos de la presente tesis, en
efecto, implementamos un modelo de emisi\'{o}n \ac{mbs} en nuestro
c\'{o}digo, para poder tratar con precisi\'{o}n la emisi\'{o}n en todos los
rangos de energ\'{\i}a de la \ac{eed}.
%%<<<<<<<<<<<<<<<<<<<<<<<<<<<<

%% Translation of RMA17, p. 1174
%%>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Para la evoluci\'{o}n de las part\'{\i}culas inyectadas en los choques,
suponemos que los procesos dominantes son el enfriamiento sincrotr\'{o}n y
la dispersi\'{o}n \ac{ic} de los fotones producidos por el prooceso
\ac{eic}. Para calcular los espectros y curvas de luz sint\'{e}ticas,
dependientes del tiempo y para m\'{u}ltiples frequencias, incluimos los
procesos de emisi\'{o}n sincrotr\'{o}n y \ac{ic} resultantes del plasma que
ha sufrido el choque. Consideramos adem\'{a}s que la l\'{\i}nea de
visi\'{o}n del observador respecto al eje del chorro forma un \'{a}ngulo
$\theta$. Una descripci\'{o}n detallada de c\'{o}mo se realiza la
integraci\'{o}n de la ecuaci\'{o}n de transferencia radiativa a lo largo de
la l\'{\i}nea de visi\'{o}n se puede encontrar en la secci\'{o}n
\Sref{sec:shells-coll-dynam}.
%%<<<<<<<<<<<<<<<<<<<<<<<<<<<<

%% Translation of the Conclusions
%%>>>>>>>>>>>>>>>>>>>>>>>>>>>>
\section{Conclusiones}
\label{sec:conclusiones}

Para poder realizar simulaciones num\'{e}ricas consistentes de
fen\'{o}menos astrof\'{\i}sicos, se necesitan herramientas computacionales
robustas f\'aciles de mantener y que sean eficientes en el uso de los
recursos computacionales disponibles. Para poder reproducir las
observaciones de los objetos astrof\'{\i}sicos en la Tierra, se deben
elegir las herramientas num\'{e}ricas adecuadas y dise\~{n}ar un algoritmo
bien estructurado. Para ello, hay que mantenerse al d\'{\i}a con los
postulados te\'{o}ricos, t\'{e}cnicas num\'{e}ricas y observaciones m\'{a}s
recientes. De lo contrario, se corre el riesgo de malinterpretar los datos.
En esta tesis hemos presentado la metodolog\'{\i}a utilizada para simular
los \acp{sed} de bl\'{a}zares, siguiendo el modelo de \ac{is}.

Aqu\'{\i} se han presentado dos proyectos de investigaci\'{o}n
complementarios. Ambos tratan el modelo de \acp{is} para los flujos
relativistas magnetizados y se aplica a los bl\'{a}zares. El primero
consisti\'{o} en la exploraci\'{o}n del espacio de par\'{a}metros haciendo
uso del c\'{o}digo para \acp{is} \citepalias{Mimica:2012aa}. Los resultados
num\'{e}ricos (p.~ej., \acp{sed} sint\'{e}ticas) se compararon con datos
observacionales \citepalias[Cap\'{\i}tulo~\ref{cha:prmint}
y][]{Rueda:2014mn}. Esta investigaci\'{o}n parti\'{o} de la hip\'{o}tesis
que la radiaci\'{o}n producida en los choques que se propagan a trav\'{e}s
de las capas magnetizadas debe contener la firma (en las \acp{sed}
observadas) del grado de magnetizaci\'{o}n de dichas capas. Hemos
encontrado que, de hecho, la magnetizaci\'{o}n juega un papel importante y
que, en efecto, deja patrones f\'{a}cilmente identificables en las
\acp{sed} y en el plano $A_C$--$\nusynobs$. En este \'{u}ltimo, hemos
identificado los par\'{a}metros cuyas variaciones explican la secuencia de
los bl\'{a}zares.

El segundo proyecto de investigaci\'{o}n consisti\'{o} en dos subproyectos:
la inclusi\'{o}n de electrones t\'{e}rmicos en la poblaci\'{o}n de los
inyectados en el frente de choque y el tratamiento adecuado de la
emisi\'{o}n \ac{mbs} por electrones no relativistas y transrelativistas
\citep[Cap\'{\i}tulo~\ref{cha:hybdis} y][]{Rueda:2017hd}. La idea para el
primero surgi\'{o} de estudios previos sobre la trazas t\'{e}rmicas en las
curvas de luz de BRGs \citep{Giannios:2009zm}, y de resultados obtenidos
con simulaciones de part\'{\i}culas en c\'{e}ldas (\ac{pic})
\citep{Sironi:2013hf} que sugieren que una gran fracci\'{o}n de la
energ\'{\i}a disipada en choques escasamente magnetizados probablemente
termina en electrones t\'{e}rmicos. En cuanto a un tratamiento completo de
la emisi\'{o}n \ac{mbs}, la motivaci\'{o}n para esto provino del hecho de
que la potencia radiada espectral de electrones lentos y ligeramente
relativistas desarrollan patrones pr\'{a}cticamente discontinuos
\citep{Mahadevan:1996ek}, al contrario de lo que la emisi\'{o}n
sincrotr\'{o}n de electrones ultrarelativistas predice
\citep{Rybicki:1979,Jackson:1999}. Las simulaciones que incluyeron ambos
efectos revelaron que la presencia de electrones t\'{e}rmicos (adem\'{a}s
de los no t\'{e}rmicos) durante la aceleraci\'{o}n en el choque afecta
razonablemente el espectro a frecuencias entre el \'{o}ptico y los rayos
$\gamma$, mientras que las $\nusynobs$ sufre muy poca variaci\'{o}n. Esto
significa que la localizaci\'{o}n de la fuente en el plano
$A_C$--$\nusynobs$ sufrir\'{a} un desplazamiento vertical; tal vez dando
explicaci\'{o}n la dispersi\'{o}n de las fuentes en la regi\'{o}n de los
\acp{fsrq}.
%%<<<<<<<<<<<<<<<<<<<<<<<<<<<<

%% Translation of RMA14, p. 1867
%%>>>>>>>>>>>>>>>>>>>>>>>>>>>>
En el modelo est\'{a}ndar, las \acp{sed} de \acp{fsrq} y \acp{bllac} pueden
ser ajustados por dos componentes parab\'{o}licas con m\'{a}ximos
correspondientes a los picos sincrotr\'{o}n e \ac{ic}. Demostramos que las
\acp{sed} de \acp{fsrq} y \acp{bllac} dependen \'{\i}ntimamente de la
magnetizaci\'{o}n del plasma emisor. Nuestros modelos predicen una
fenomenolog\'{\i}a a\'{u}n m\'{a}s compleja que los que actualmente
est\'{a}n respaldados por los datos observacionales. Con un enfoque
conservador, esto implicar\'{\i}a que las observaciones restringen la
posible magnetizaci\'{o}n en el choque de capas que tiene lugar en las
fuentes reales a, a lo sumo, valores moderados $(\sigma \lesssim
10^{-1})$. Aunque tambi\'{e}n hemos demostrado que si el valor del factor
de Lorentz del fluido de las capas es lo suficientemente alto
(p. ej. $\GammaR$ y $\Delta g$), magnetizationes $\sigma \simeq 1$
tambi\'{e}n son compatibles con la doble joroba en los espectros. Por
tanto, no podemos descartar del todo la posibilidad de que algunas fuentes
sean muy ultrarelativistas tanto en el sentido cinem\'{a}tico como en el de
su magnetizaci\'{o}n.

Encontramos que \acp{fsrq} tienen propiedades observacionales al alcance de
modelos con campos magn\'{e}ticos despreciables o moderados. La
dispersi\'{o}n de los \acp{fsrq} observados en el plano $A_C$--$\nusynobs$
puede explicarse por ambas variaciones de los par\'{a}metros
intr\'{\i}nsecos de los grumos ($\Delta g$ y $\GammaR$ muy probable), y de
los extr\'{\i}nsecos (la orientaci\'{o}n de la fuente respecto al
observador en la Tierra). Por su parte, \acp{bllac} con frecuencias
sincrotr\'{o}n m\'{a}ximas $\nusynobs \lesssim$ \SI{1e16}{\hertz} y
par\'{a}metro de dominancia Compton $0.1 \gtrsim A_C \gtrsim 1$ despliega
propiedades que se pueden reproducir con modelos de moderada y uniforme
magnetizaci\'{o}n $(\sigmaL = \sigmaR = 10^{-2})$. Por lo tanto,
encontramos que una buena fracci\'{o}n de la secuencia de los bl\'{a}zares
puede ser explicada en t\'{e}rminos de diferentes magnetizaciones
intr\'{\i}nsecas de las capas colisionantes.
%%<<<<<<<<<<<<<<<<<<<<<<<<<<<<

%% Translation of RMA17, p. 1179
%%>>>>>>>>>>>>>>>>>>>>>>>>>>>>
En la presente tesis, introducimos una distribuci\'{o}n de electrones
t\'{e}rmica y no t\'{e}rmica (h\'{\i}brida) en el modelo \acp{is} para
bl\'{a}zaress. Para explicar el hecho de que la componente t\'{e}rmica de
la \ac{hd} se extiende a valores bajos del factor de Lorentz de los
electrones, tambi\'{e}n hemos desarrollado un c\'{o}digo
ciclo-sincrotr\'{o}n que nos permite calcular la emisi\'{o}n no t\'{e}rmica
de electrones con un factor de Lorentz arbitrario. Se muestra que nuestro
m\'{e}todo para el tratamiento de la evoluci\'{o}n temporal de la \ac{hd} y
el c\'{a}lculo de la emisi\'{o}n \ac{mbs} se puede realizar de manera
eficiente y con suficiente precisi\'{o}n. El m\'{e}todo se implementa como
una generalizaci\'{o}n del c\'{o}digo num\'{e}rico de
\citetalias{Mimica:2012aa}.
%%<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%% Translation of Future work
%%>>>>>>>>>>>>>>>>>>>>>>>>>>>>
\section{Trabajo futuro}
\label{sec:trabajo-futuro}

Respecto al desarrollo de nuestro c\'{o}digo \chamba~y la nueva funci\'{o}n
$RMA$ (v\'{e}ase la \Eref{eq:new-rma}), el valor \'{o}ptimo del
par\'{a}metro $\acoff$ debe ser estimado. Para lograr este objetivo,
planeamos calcular y comparar sistem\'{a}ticamente el error relativo
acumulado entre las emisividades \ac{mbs} usando~\eqref{eq:new-rma}
y~\eqref{eq:emiss-coef-iso-discrete}, para diferentes valores de
$\acoff$. Esperamos obtener un valor que nos ayude a reducir las
ramificaciones en~\eqref{eq:new-rma}. En otras palabras, nuestra
intenci\'{o}n es encontrar un ajuste que apropiadamente tenga en cuenta el
corte.

Con respecto al modelo de \ac{is} para bl\'{a}zares, tenemos como objetivo
claro realizar exploraciones adicionales de los efectos en las \acp{sed} de
bl\'{a}zares debido a las variaciones en la magnetizaci\'{o}n y \acp{hd},
incluyendo la emisi\'{o}n \ac{mbs} completa, para lo cual pretendemos
incluir efectos como la absorci\'{o}n y emisi\'{o}n \ac{eic}. Los procesos
microf\'{\i}sicos en este escenario son cr\'{\i}ticos para entender
c\'{o}mo se aceleran las part\'{\i}culas en los frentes de ondas de choque
relativistas. Los par\'{a}metros microf\'{\i}sicos $a_\acc$ y $\Delta_\acc$
que regulan la escala de tiempo de aceleraci\'{o}n y el tama\~{n}o de la
regi\'{o}n de aceleraci\'{o}n de los electrones, respectivamente, ya han
empezado a ser estudiados y hemos encontrado nuevos efectos en las curvas
de luz de blazars. Sin embargo, queda pendiente un estudio definitivo y
esperamos que estos resultados sean parte de alguna publicaci\'{o}n
futura. Por otra parte, no s\'{o}lo est\'{a} en curso las variaci\'{o}n de
par\'{a}metros de la versi\'{o}n actual del c\'{o}digo para bl\'{a}zaress,
sino que ya estamos trabajando en la implementaci\'{o}n del enfriamiento
\ac{ssc} \citep[p.~ej.][]{Chiaberge:1999gh}, as\'{\i} como tambi\'{e}n el
c\'{a}lculo del coeficiente de absorci\'{o}n \citep{Ghisellini:1991sv} del
proceso \ac{mbs} para su aplicaci\'{o}n directa a las simulaciones de
bl\'{a}zars.

En el contexto astrof\'{\i}sico, y en relaci\'{o}n con los flujos
relativistas en particular, la presente tesis establece un punto de
arranque para la exploraci\'{o}n de fen\'{o}menos m\'{a}s all\'{a} de
bl\'{a}zars. Por ejemplo, el aplanamiento tard\'{\i}o de las curvas de luz
de las posluminiscencias de \acp{grb}, en la llamada etapa newtoniana
profunda \citep[p.~ej.][]{Huang:2003ch,Sironi:2013gi}, es un escenario
ideal en el que las herramientas num\'{e}ricas desarrolladas en la presente
tesis podr\'{\i}an proporcionar un medio para explorar los remanentes de
\acp{grb}. Y en efecto lo es porque en esta etapa los electrones acelerados
en el frente de choque de la onda expansiva se vuelven
transrelativistas \citep{Sironi:2013gi}, y por lo tanto los efectos tanto de
la emisi\'{o}n \ac{mbs} como de las \acp{hd} pueden ser relevantes.

Hace falta igualmente realizar m\'{a}s comparaciones de las simulaciones
con observaciones para probar este modelo. Sin embargo, dado que la escala
de la regi\'{o}n de la que se compila la emisi\'{o}n en nuestras
simulaciones es sub-parsec, hacen falta observaciones de las regiones de
los bl\'{a}zares que emiten a altas energ\'{\i}as tengan una mayor
resoluci\'{o}n para poder hacer una comparaci\'{o}n adecuada de, p.~ej.,
sus respectivas \acp{sed}.











%%%%%%%% TRANSLATIONS FROM GOOGLE
%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

% Para calcular espectros de longitud de onda y longitudes de onda
% m\'{u}ltiples sint\'{e}ticas dependientes del tiempo, se supone que los
% procesos de emisi\'{o}n dominantes resultantes del plasma con choque son el
% sincrotr\'{o}n, el EIC y el SSC. El componente EIC es el resultado de la
% dispersi\'{o}n ascendente de fotones del infrarrojo cercano (probablemente
% emitidos por un toro polvoriento alrededor del motor central del blazar o
% de la regi\'{o}n de la l\'{\i}nea ancha) por los electrones no t\'{e}rmicos
% existentes en el chorro. Consideramos adem\'{a}s que la l\'{\i}nea de
% visi\'{o}n del observador hace un \'{a}ngulo ? con el eje del chorro.

% Modelamos la din\'{a}mica de la c\'{a}scara y las propiedades de choque en
% chorros blazar como en MA12. Suponiendo un flujo de salida cil\'{\i}ndrico
% y despreciando la expansi\'{o}n lateral del chorro (que desempe\~{n}a un
% papel insignificante en los chorros de blazes, v\'{e}ase por ejemplo Mimica
% et al., 2004), podemos simplificar el problema de las c\'{a}scaras
% colisionantes a una interacci\'{o}n unidimensional de dos c\'{a}scaras
% cil\'{\i}ndricas, El radio de secci\'{o}n R y el grosor r. El factor
% Lorentz de la c\'{a}scara m\'{a}s lenta (derecha) se denomina R, mientras
% que la c\'{a}scara m\'{a}s r\'{a}pida (izquierda) se mueve con L = (1 + g)
% R. En la expresi\'{o}n anterior, g representa el factor de Lorentz relativo
% entre las conchas que interact\'{u}an. Asumimos que las c\'{a}scaras son
% inicialmente fr\'{\i}as, de modo que la presi\'{o}n t\'{e}rmica del fluido
% (P) a la relaci\'{o}n de densidad de energ\'{\i}a en reposo ?: = P / ?c2 ,
% donde ? es la densidad de masa de reposo fluida. La magnetizaci\'{o}n del
% casco es controlada por un par\'{a}metro ?: = B2 / (4? 2?c2), donde B es la
% fuerza del campo magn\'{e}tico a gran escala (medido en el marco del
% laboratorio), que en nuestro modelo se supone que es perpendicular a la
% carcasa Direcci\'{o}n de propagaci\'{o}n.

% La evaluaci\'{o}n num\'{e}rica de la emisi\'{o}n de MBS (ecuaci\'{o}n 25)
% es muy dif\'{\i}cil porque se necesita realizar una integral sobre una suma
% infinita de funciones Jm y sus derivadas Jm '. Se han utilizado varias
% t\'{e}cnicas para calcular dicha integral. Una f\'{o}rmula anal\'{\i}tica
% aproximada fue encontrada por Petrosian (1981) usando la pendiente m\'{a}s
% escarpada

% La evaluaci\'{o}n num\'{e}rica de la emisi\'{o}n de MBS (ecuaci\'{o}n 25)
% es muy dif\'{\i}cil porque se necesita realizar una integral sobre una suma
% infinita de funciones Jm y sus derivadas Jm '. Se han utilizado varias
% t\'{e}cnicas para calcular dicha integral. Una f\'{o}rmula anal\'{\i}tica
% aproximada fue encontrada por Petrosian (1981) usando el m\'{e}todo de
% pendiente m\'{a}s empinada para lograr una buena precisi\'{o}n en los
% reg\'{\i}menes ciclotr\'{o}n y sincrotr\'{o}n, pero los errores relativos
% en el r\'{e}gimen intermedio fueron entre 20\% y 30\%. En los trabajos
% subsiguientes, se ha hecho un esfuerzo para calcular con exactitud la
% emisividad de MBS en toda la gama de frecuencias (para una revisi\'{o}n
% corta, v\'{e}ase, por ejemplo, Leung et al., 2011).

% El m\'{e}todo que seguimos consiste en integrar primero la ecuaci\'{o}n
% (25) trivialmente sobre ??, explotando la presencia de la funci\'{o}n
% ?. Este es el mismo primer paso que el empleado en Leung et al. (2011),
% pero para los factores de Lorentz ?. Entonces, a partir de la condici\'{o}n
% de resonancia (ecuaci\'{o}n 24), encontramos l\'{\i}mites superior e
% inferior para la suma sobre arm\'{o}nicos.

% Con el fin de calcular la emisividad (ecuaci\'{o}n 19), primero calcular
% $X^2 I1 (X, ?)$ y almacenar en una matriz bidimensional. Para minimizar
% los problemas num\'{e}ricos causados ??por una fuerte ca\'{\i}da en la
% potencia radiada a factores bajos de Lorentz (manteniendo X constante), se
% construye una matriz de corte {?min} (v\'{e}ase el Ap\'{e}ndice B). La
% integraci\'{o}n sobre ? en la ecuaci\'{o}n (30) se realiza utilizando una
% cuadratura de Gauss-Legendre y considerando que la emisi\'{o}n es
% isotr\'{o}pica. En esta etapa, la evaluaci\'{o}n ? = 0 se evit\'{o} tomando
% un n\'{u}mero par de puntos nodales (espec\'{\i}ficamente, 120 nodos). Para
% completar la matriz, se calculan los coeficientes de Chebyshev en la
% direcci\'{o}n ? de $X^2 I1 (X, ?)$.

% Se construye una tabla de interpolaci\'{o}n y se utiliza posteriormente
% para calcular num\'{e}ricamente la emisividad (ecuaci\'{o}n 19). Se
% discrimina la HD por tessellating en un gran n\'{u}mero de Lorentz factor
% intervalos cuyos l\'{\i}mites anotar con {?i} Mi = 1. Obs\'{e}rvese que el
% valor m\'{a}s peque\~{n}o y m\'{a}s grande del tesselado del factor Lorentz
% coincide con las definiciones dadas en la ecuaci\'{o}n (18). Para la
% comodidad num\'{e}rica y la eficiencia, en cada intervalo, se aproxima el
% EED por una funci\'{o}n de ley de potencia (con un \'{\i}ndice de poder-ley
% qi), ya que, para esta forma particular, es posible realizar una parte del
% c\'{a}lculo que dr\'{a}sticamente Reduce el tiempo computacional. A
% continuaci\'{o}n, utilizamos la nueva tabla de interpolaci\'{o}n para
% calcular la emisividad a una frecuencia arbitraria como se describe a
% continuaci\'{o}n.

% Calculamos la integral que depende de los tres par\'{a}metros de la
% ecuaci\'{o}n (38) recurriendo a un m\'{e}todo de cuadratura est\'{a}ndar de
% Romberg para cada valor del triplete (?, X, q). De la misma manera, como
% con la ecuaci\'{o}n (30), se construye una matriz tridimensional para I3
% (?, X, q) con los coeficientes de Chebyshev en la direcci\'{o}n ? para
% construir una tabla de interpolaci\'{o}n para I2 (en adelante disTable).
% La integral sobre los factores de Lorentz se realiz\'{o} para todos los
% valores de X y q utilizando una rutina de integraci\'{o}n de
% Romberg. An\'{a}logamente a I 1 (v\'{e}ase el Ap\'{e}ndice B), los
% polinomios de Chebyshev se construyeron en la direcci\'{o}n ?.

%%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "chap"
%%% End:
