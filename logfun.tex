\chapter{Numerical logarithmic functions}
\label{cha:numer-logar-funct}

While performing the integrals in the \ac{ssc} and \ac{eic}
calculations we may encounter cases where the logarithmic function has
an argument \num{\sim{}1}. In order to avoid numerical pathologies and
the loss of precision that may be compiler- or library-dependent, the
logarithmic functions were numerically extended on the basis of their
Taylor series expansion around $1$. In this way we can control their
behaviour regardless of the way that a particular compiler or library
treats the case of an argument being close to $1$.  In
\tref{tab:log-defns} we describe the aforementioned functions, showing
their respective expansion and tolerance functions (the second term of
the series expansion). If the tolerance is below some threshold
$\epslog$ then we use the series expansion up to the first term,
otherwise the intrinsic function of the programming language (i.e.,
\texttt{FORTRAN}, \texttt{C++} or \texttt{Python}) is called.
%
\begin{sidewaystable}
  \centering
  \begin{tabular}{lcccccc}
    \toprule
    Name
    & Function
    & Series expansion
    & Tolerance function
    & $> \epslog{}$ & $\leq \epslog{}$ & $\epslog{}$ \\
    \midrule
    % LOG1
    $\mathtt{Log1}$
    & $\log$ & $(x - 1) - \frac{1}{2} {(x - 1)}^2 + \bigO{(x - 1)}^3$
    & $0.5 {(x - 1)}^2$ & $\log(x)$ & $x - 1$ & \num{1e-9} \\[7pt]
    % LOG2
    $\mathtt{Log2}$
    & $\log^2$ & ${(x - 1)}^2 - {(x - 1)}^3 + \bigO{(x - 1)}^4$
    & ${(x - 1)}^3$ & $\log^2 (x)$ & ${(x - 1)}^2$ & \num{1e-9} \\[7pt]
    % LOG3
    $\mathtt{Log3}$
    & $\log^3$ & ${(x - 1)}^3 - \frac{3}{2} {(x - 1)}^4 + \bigO{(x - 1)}^5$
    & $1.5 {(x - 1)}^4$ & $\log^3(x)$ & ${(x - 1)}^3$ & \num{1e-9} \\[7pt]
    % LOG4
    $\mathtt{Log4}$
    & $\log^{4}$ & ${(x - 1)}^{4} - 2 {(x - 1)}^{5} + \bigO{(x - 1)}^6$
    & $2.0 {(x - 1)}^5$ & $\log^{3}(x)$ & ${(x - 1)}^4$ & \num{1e-9} \\[7pt]
    % LOG5
    $\mathtt{Log5}$
    & $\log^5$ & ${(x - 1)}^5 - \frac{5}{2} {(x - 1)}^6 + \bigO{(x - 1)}^7$
    & $2.5 {(x - 1)}^6$ & $\log^5(x)$ & ${(x - 1)}^5$ & \num{1e-9} \\[7pt]
    \bottomrule
  \end{tabular}
  \caption[Alternative logarithmic functions]{Alternative logarithmic
    functions. In this table we outline the functions implemented in
    the code, and their corresponding analytic function. These
    functions were defined making use of a Taylor series expansion
    around 1 (third column). Here we also show the tolerance
    function (fourth column) for each function, which
      corresponds to the second term in the series expansion
      Fifth and sixth columns show
      which expressions is used if the tolerance function returns a value above of
      below the tolerance $\epslog$. The last column shows
      the value of $\epslog$ used for each function.}
\label{tab:log-defns}
\end{sidewaystable}

% In the upper panel of \fref{fig:logfun-relerr} we have plotted the five new
% defined logarithmic functions with a tolerance $\epslog = 10^{-5}$ (dashed
% lines), compared to \texttt{NumPy}\footnote{\texttt{NumPy} is a library
%   written for the \texttt{Python} programming language. It is especially
%   well suited for the scientific computing. For more information see
%   http://www.numpy.org/} intrinsic logarithmic functions (solid lines). The
% black, red, blue, green and magenta lines correspond to the functions
% $\log, \log^2, \log^3, \log^4$ and $\log^5$, respectively. In the lower
% panel we show their relative errors. The reason we use these new functions
% is not only due to the decrease in the computational time\footnote{In
%   computational terms, simple operations like sum, multiplication and even
%   powers to an integer value are much faster to compute than complex
%   intrinsic or user-defined functions.} for $x\approx 1$ but also in the
% fact that even for a rather coarse tolerance like $\epslog = 10^{-5}$, the
% relative error in most cases is below
% \SI[number-unit-product={}]{1}{\percent} for $x - 1 \lesssim 10^{-2}$,
% which makes them accurate enough approximations. This will come helpful at
% the moment of integrating functions which contain powers of the
% logarithm. \maacorr{The motivation for introducing these functions was, as
%   you say at the beginning of the paragraph: ``to avoid numerical
%   pathologies and the loss of precision the logarithmic
%   functions''. However, at the end of the paragraph, the justification is
%   different and likely contradictory: it turns out that the main reason to
%   use these functions is that they are more computationally efficient. If
%   it is so, you should begin the paragraph introducing the correct
%   motivation for this kind of approximations.}
%
% \begin{figure}[ht]
%   \centering
%   \includegraphics[width=\textwidth]{chambaFigures/logFunctions.eps}
%   \caption[Logarithmic functions and relative errors]{Alternative
%     logarithmic functions and their relative errors. In the present figure
%     We show the comparison between the logarithmic functions defined
%     in \tref{tab:log-defns}, but using $\epslog = 10^{-5}$ for illustration
%     purposes. In the upper panel we plot the (\texttt{NumPy}) intrinsic
%     functions $\log, \log^2, \log^3, \log^4$ and $\log^5$ in solid lines,
%     and $\mathtt{Log1}, \mathtt{Log2}, \mathtt{Log3}, \mathtt{Log4}$ and
%     $\mathtt{Log5}$ in dashed black, red, blue, green and magenta lines,
%     respectively. In the lower panel we plot the relative error between the
%     new logarithmic functions and the intrinsic functions. \maacorr{I
%       do not get the point of this comparison. It turns out that the
%       approximated functions are WORST than the intrinsic one. The
%       motivation to introduce the new functions was to fix numerical
%       pathologies and a loss of precision. I do not see in this plot
%       any pathology of the intrinsic functions...}}
% \label{fig:logfun-relerr}
% \end{figure}
% \jmcorr{**note to myself** Mention that we found anomalies in
% Mathematica, although Fortran and Numpy provide intrinsic functions which
% are accurate to machine precision**.}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "phd"
%%% End:
