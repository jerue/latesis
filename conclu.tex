\chapter{Conclusions and future work}
\label{cha:conclusions}

In the present chapter we enclose the overall conclusions and the outlook
of the present thesis We point out that more detailled conclusions have
been spelled out at the end of each individual chapter. Here we recap the
most salient features of this research.

\section{Conclusions}
\label{sec:conclusions}

To be able to perform consistent numerical simulations of
astrophysical phenomena robust, easy to maintain and resource-saving
%\maacorr{in which
%  sense are they sustainable?? do you mean ``computationally easy to
%  maintain'' or something similar?}\jmcorr{I meant in the sense of both
%  computationally maintainable and affordable to our computer
%  resources.} --> MAA: then do not use sustainable. It means other things.
computational tools are necessary. In order to be able to reproduce the
observations of astrophysical objects on Earth, adequate numerical tools
must be chosen and a well structured algorithm needs to be designed. For
that purpose, one has to constantly keep up-to-date with state of the art
of the theory, numerical techniques and observations. Otherwise, one runs
the risk of misinterpreting the data and/or failing to correctly interpret
them. In this thesis we have presented the methodology used to simulate the
\ac{sed} of blazars, following the shock-in-jet model.

Two complementary research projects have been presented here. They deal
with the internal shock model for magnetized relativistic outflows, and are
applied to blazars. The first one consisted of a parameter space scan that
was performed using the \ac{is} code \citepalias{Mimica:2012aa}. The
numerical results (e.g., synthetic \acp{sed}) were compared with
observational data \citepalias[\Cref{cha:prmint} and][]{Rueda:2014mn}. This
investigation stem from the hypothesis that the radiation produced at the
shocks propagating through the magnetized shells should contain a signature
(in the observed \ac{sed}) of the degree to which the shells are
magnetized. We have found that, indeed, the magnetization plays an
important role, leaving easily identifiable patterns in the \acp{sed} and
in the $A_C$--$\nusynobs$ plane. In the latter, we identified the
parameters whose variations account for the blazar sequence.

The second research project consisted of two sub-projects: the inclusion of
thermal electrons to the population of injected ones at the shock front,
and the proper treatment of \ac{mbs} emission by nonrelativistic and
transrelativistic electrons \citepalias[\Cref{cha:hybdis}
and][]{Rueda:2017hd}. The idea for the former arose from previous studies of
the thermal signatures in the light curves of \acp{grb}
\citep{Giannios:2009zm}, and from results out of \ac{pic} simulations
\citep{Sironi:2013hf}, which suggest that a large fraction of the energy
dissipated in weakly magnetized shocks probably goes into a distribution of
thermal electrons. This distribution extends from very low electron
energies (i.e., Lorentz factors) to moderate electron energies, where it
smoothly joins a power-law, high-energy tail. Including a full treatment of
the \ac{mbs} emission, was triggered by two basic facts. First of all, for
consistency. Since the spectral radiated power of slow and mildly
relativistic electrons develop a shape far from continuous
\citep{Mahadevan:1996ek}, contrary to what the synchrotron emission of
ultrarelativistic electrons predicts \citep{Rybicki:1979,Jackson:1999}. On
the second place, because actual particle acceleration in shocks likely
produces a broad electron distribution extending from very low electron
Lorenz factors ($\gamma \gtrsim 1$) as we have commented. Furthermore, even
if initially the accelerated electrons possess large Lorentz factors, they
may eventually be cooled down by radiative or adiabatic loses. Hence, the
lower energy end of the electron energy distribution may approach the limit
$\gamma \sim 1$. In that limit, the standard (ultrarelativistic)
synchrotron approximation breaks down and a complete MBS treatment becomes
mandatory. We stress that, while the former effect could be ignored if the
electron energy distribution is set up with a sufficiently large minimum
Lorentz factor, the latter effect is almost unavoidably hit in evolutionary
models. One only needs to wait long enough until the adiabatic expansion of
the injected particles cools them down, or incorporate strong enough
magnetic fields for that to happen. The simulations that include these two
effects revealed that the presence of thermal (in addition to the
nonthermal) electrons during the shock acceleration moderately affects the
spectrum at optical-to-$\gamma$ ray frequencies, while $\nusynobs$ suffers
little variation. This means that the source location in the
$A_C$--$\nusynobs$ plane will suffer a vertical displacement; perhaps
explaining a scatter of the \ac{fsrq} region and \ac{lsp} region.


\section{Future work}
\label{sec:future-work}

On the code-development side and regarding the new $RMA$
function~\eqref{eq:new-rma}, the optimal value of the parameter $\acoff$
must be estimated. To achieve this goal, we plan to calculate and compare
systematically the accumulated relative error between the \ac{mbs}
emissivities using~\eqref{eq:new-rma}
and~\eqref{eq:emiss-coef-iso-discrete}, for different values of
$\acoff$. We expect to obtain a value which can help us reducing the
branching of~\eqref{eq:new-rma}. In other words, our intention is to find a
fit which correctly takes into account the cut-off.

Regarding the \ac{is} model for blazars, further examination of the effects
of the variations in magnetization and \acp{hd} including the full \ac{mbs}
emission on blazars \acp{sed} are a clear goal, for which we intend to
include the effects of absorption and \ac{eic} emission. Microphysical
processes in this scenario are critical to understanding how are the
particles being accelerated at relativistic shocks. The microphysical
parameters $a_\acc$ and $\Delta_\acc$, which regulate the acceleration
timescale and the size of acceleration region of the electrons,
respectively, have already been studied and show novel effects in the
light-curves of blazars. However, decisive studies are still pending and we
expect these results to be part of some future publication soon. However,
not only are the variations of parameters of the current version of the
blazars code in the pipeline, but we are already working on the
implementation of the \ac{ssc} cooling \citep[e.g.][]{Chiaberge:1999gh}, as
well as on the calculation the absorption coefficient
\citep[e.g.][]{Ghisellini:1991sv} out of \ac{mbs} process for direct
application to simulations of blazars.

In the astrophysical context, and regarding relativistic outflows in
particular, the present thesis sets a landmark for the exploration of
phenomena beyond blazars. For instance, the late-time flattening of
light curves of \acp{grb} afterglows, in the so called deep Newtonian
phase \citep[e.g.][]{Huang:2003ch,Sironi:2013gi}, is an ideal scenario
in which the numerical tools showed in previous chapters could afford
means to explore \ac{grb} remnants. This is because at this stage the
accelerated electrons at the shock front of the blast wave become
transrelativistic \citep{Sironi:2013gi}, and thus the effects of both
\ac{mbs} and \ac{hd} may become relevant.

Likewise, further comparisons with observations must be performed to prove
the model. However, since the emission region in our simulations is in
sub-parsec scales, data from higher resolved observations from the
high-energy emission region of blazars is needed in order to carry out
proper comparisons of, e.g., the \acp{sed}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "phd"
%%% reftex-default-bibliography: ("./phd.bib")
%%% End:
