\section{SSC Cooling}
\label{sec:cooling-part}

The loss of energy of electrons subject to radiative energy losses
\citep{Gratton:1972he} is given by \Eref{eq:ener-loss}. The radiative
cooling processes considered in the \ac{is} model usually are the
synchrotron and \ac{eic}, if present (see~\eqref{eq:el-cool-fact}). In the
\rplc{present}{this} section we will describe the numerical treatment of an
additional radiation process: the \ac{ssc} cooling, which is a dynamic and
non-linear process \citep[e.g.][]{Zacharias:2010aa}

Let us consider an optically thin spherical region of radius $R$ in
which at a time $\comv{t} = 0$ charged particles are injected at a
rate $\difd n / \difd \comv{t} \difd \gammap$, immersed in a uniform
magnetic field $\comv{B}$. \addnew{Furthermore, let us assume that
  initially there are no target photons for inverse Compton
  scattering.}  \maacorr{agree??} If we consider synchrotron and
\ac{ssc} losses, the cooling factor in \Eref{eq:ener-loss} reads
\maacorr{Please, also note the explicit dependences introduced in the
  the loss term}
%
\begin{equation}
  \label{eq:cool-fact}
  \nu_0(\comv{t}) = \dfrac{4}{3} c \sigT \dfrac{\uBp + \usscp(\gammap,\comv{t})}{\mel c^2}.
\end{equation}
\maacorr{is this formula correct in the non-relativistic limit? I am a
bit confused here. Where are the light crossing time effects present
here? If $R=10^{17}$\,cm, then $T_{\textrm{cross}}=R/c\sim 3\cdot 10^6\,$s. This means
that synchrotron photons produced in one end of the spherical region
will not reach the center (or viceversa) until a time which is longer
than any of the displayed times in, e.g., \fref{fig:pwl-n-sscCool}.}
%
where $\uBp = \comv{B}^2 / 8 \piup$ and \citep{Rybicki:1979}
\maacorr{really in \citep{Rybicki:1979}? in which equation? I have
  found something similar in \cite{Dermer:2009}, Eq.7.118 and also in
  Chiaberge \& Guisellini (1999; MNRAS, 306, 551) -I would refer to
  the latter paper-. Please, also note
  the explicit dependences introduced in the integral limits and in
  the loss term}
%
\begin{equation}
  \label{eq:uSSC}
  \usscp(\gammap,\comv{t}) = \dfrac{1}{c} \int_{\nu_{\textrm{s ,min}}'}^{\nu_{\max}'(\gammap)} \difd \nup
  \int_{\partial \Omega} \difd \Omega \int_0^R \difd s \jnucom(\comv{t}) = \dfrac{4
    \piup R}{3 c} \int_{\nu_{\textrm{s ,min}}'}^{\nu_{\max}'(\gammap)} \jnucom(\comv{t}),
\end{equation}
%
\addnew{where $\nu_{\textrm{s, min}}'$ ($\nu_{\textrm{s, max}}'$) is
  the minimum (maximum) synchrotron emitted frequency and
  $\nu_{\max}'(\gammap)=\min \{\nu_{\textrm{s, max}}', 3\mel
  c^2/(4h\gammap)\}$, and $h$ is the Planck constant.}
\maacorr{Please, check whether you AGREE. These limites are form
  Chiaberge \& Guisellini (1999). In the draft I was correcting, the
  maximum and minimum frequencies in the integral were not even specified.}

By splitting the emissivity in $N_\nup$ intervals the integral over
frequency is performed assuming that in the interval
$[\nu_i', \nu_{i + 1}'], i = 1, \ldots, N_\nup, \jnucom \propto \nup^{-s_i}$, so
that, using~\eqref{eq:ibp-integ-func},
%
\begin{equation}
  \label{eq:jnu-int}
  \int_{\nu_i}^{\nu_{i + 1}'} \difd \nup \jnucom \approx j_{\nup,i}' \nu_i'
  P\left( \dfrac{\nu_{i + 1}'}{\nu_i'}, s_i \right).
\end{equation}
%
We can use the previous equation to compute the total \ac{ssc} energy
density at a time $\comv{t}$ is
%
\begin{equation}
  \label{eq:tot-ussc}
  \usscp(\gammap,\comv{t}) = \dfrac{4 \piup R}{3 c} \sum_{i = 1}^{N_\nup} j_{\nu, i}'(\comv{t}) \nu_i'
  \PP\left( \dfrac{\nu_{i + 1}'}{\nu_i'}, s_i\right).
\end{equation}
\maacorr{are the exponents of $\nu_i'$ in \Eref{eq:jnu-int} and
  \Eref{eq:tot-ussc} correct? Please, check!}

\subsection[SSC cooling of a power-law distribution]{SSC cooling of a
  power-law distribution of charged particles}
\label{sec:ssc-pwl}

\maacorr{As discussed, you should set up two cases in this
  section. The first one can be the one you have set up here, where
  SSC loses are negligible compared with synchrotron loses. The second
  one should be the complementary case in which
  $\usscp\gg \uBp$. Furthermore, note that you have a time
  scale to compare with in your models, namely, the light crossing
  time of the radius of the emitting sphere,
  $T_{\textrm{cross}}=R/c\sim 3\times 10^6\,$s (note that to make a
  proper comparison, we shall compute the crossing time in the
  comoving frame; there should be a simple relation
  $T_{\textrm{cross}}'\sim T_{\textrm{cross}}/Gamma$, where $\Gamma$ is
  the expansion Lorentz factor of the spherical blob). It would be worth to
  show the evolution of the \ac{eed} and its corresponding emissivity
  at times of, e.g.,
  $10^{-2}T_{\textrm{cross}}, 10^{-1}T_{\textrm{cross}},
  T_{\textrm{cross}}, 10 T_{\textrm{cross}}$.}

Making use of the \rplc{finite-time}{} particles evolution scheme developed
in \Sref{sec:power-law-source} we performed a series of tests in order to
gain an insight into the effects of the \ac{ssc} cooling on the evolution
of both the \ac{eed} and the emissivity from the moment of the injection,
$\tcom = 0$, until a time $\tcom_{\max}$. Since $\usscp$ depends on the
dimensions of the system, two different sizes of our system were
considered: $R = \SIlist{1e17;1e20}{\centi\meter}$. The former corresponds
(to within an order of magnitude) to the size of the shocked region in the
\ac{is} model of blazars, whereas the latter better describes the size of a
blazar jet. The observation frequencies are \SIrange{1e5}{1e15}{\hertz}. As
we will see in the following, these tests will prove valuable for testing
as well the aforementioned code \chamba. In
Figures~\ref{fig:pwl-n-sscCool}--\ref{fig:hyb-jnu-sscCool} we show the
evolution of the injected distribution of particles and emissivity. The
cases of larger emission region are represented with dashed lines while
solid and dotted lines corresponds to emission region with
\rplc{shorter}{smaller} radius. Both solid and dashed lines show the
results of the simulations using \chamba\ while dotted lines correspond to
the simulations in which the standard synchrotron treatment was
employed. In all cases $\comv{B} = \SI{10}{\gauss}$.
%
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{chambaFigures/sscCool-dis-pwl.eps}
  \caption[Evolution of a \rplc{hybrid}{non-thermal} EED subject to SSC cooling]{Evolution of a
    \rplc{hybrid}{non-thermal (power-law)} \ac{eed} subject to SSC cooling. For different evolutionary
    times we show the normalized number density of electrons as a function
    of their Lorentz factor. \addnew{The initial distribution is
      characterized by $q = 2.2, \gpmin = 10$ and
$\gpmax = 10^4$. The magnetic field strength is set to $\comv{B} =
\SI{10}{\gauss}$. Solid and dashed lines correspond to models with $R
= \SIlist{1e17;1e20}{\centi\meter}$, respectively. Note that there is
basically no difference in the \ac{eed} as a function of the system
size, and thus both lines overlap} \maacorr{correct? Also, replace in
the y-label and in the legends:
$t\Rightarrow \tcom$}.}
\label{fig:pwl-n-sscCool}
\end{figure}
%
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{chambaFigures/sscCool-emiss-pwl.eps}
  \caption[Evolution of a \rplc{hybrid}{non-thermal} EED subject to
  SSC cooling]{Evolution of a \rplc{hybrid}{non-thermal (power-law)}
    \ac{eed} subject to SSC cooling. Shown is the emissivity
    (normalized to the electron number density) as a function of the
    observation frequency. Different evolutionary times are shown in
    different colors. The dashed lines \maacorr{which ``dashed lines''
      do you refer to? Do you perhaps mean ``dotted lines''?} show the
    emission for $R=10^{20}$\,cm. \maacorr{Where are the green, yellow and purple solid
      lines?? Are they below the scale displayed? I can only see the
      dotted color lines corresponding to the aforementioned
      colors. ALSO: replace $t\Rightarrow \tcom$, $j_\nu\Rightarrow
      \jnucom$ and $\nu\Rightarrow \nup$}}
\label{fig:pwl-jnu-sscCool}
\end{figure}

\rplc{In the first case of}{We inject in the computational domain a
  pure non-thermal (power-law)} \ac{eed} \rplc{we injected a power-law
  distribution of electrons}{}, given by~\eqref{eq:pwl-dist} with
$q = 2.2, \gpmin = 10$ and $\gpmax = 10^4$, and let the system evolve
until $\comv{t}_{\max} = \SI{1d6}{\second}$. In
Figures~\ref{fig:pwl-n-sscCool} and~\ref{fig:pwl-jnu-sscCool} the
number density of electrons and emissivity are shown,
respectively. What we can learn from these figures is that the
$\usscp$ \rplc{therm}{term} in the cooling coefficient is
negligible compared to the $\uBp$, even though the difference in
radius of the larger emission region (dashed lines) is three orders of
magnitude larger. This is due to the fact that the
\addnew{\ac{mbs}} emissivity \rplc{does not contribute enough
  to}{is extremely small in the computed frequency bands making} the
\ac{ssc} cooling term (see \fref{fig:pwl-jnu-sscCool})
\addnew{irrelevant for this \ac{eed} and system size}. This applies
also to the standard synchrotron treatment (dotted lines
in \fref{fig:pwl-jnu-sscCool}). In \fref{fig:pwl-jnu-sscCool} we can
notice that the simulations in which \rplc{\chamba}{the complete
  \ac{mbs} emissivity} was employed, the expected drop in the
emissivity is noticeable for
$\nup \lesssim \SI{2e6}{\hertz}\approx \nug$.

\subsection[SSC cooling of a hybrid distribution]{SSC cooling of a hybrid
  distribution of charged particles}
\label{sec:ssc-hyb}

For the case of a \ac{hd} of electrons injected (see
\Sref{sec:hyb-ther-nonther}) we let the system evolve to a time
$t_{\max}' = \SI{5d5}{\second}$ and the Lorentz factor spectrum
\numrange{1.01}{1e4}. For the non-thermal component we use
$q = 2.2, \gpmin = 100, \gpmax = 10^4$ and constitutes a portion
$\zetae = 0.6$ of the total. For the thermal component we set
$\Thetae = 50$.

In Figures~\ref{fig:hyb-n-sscCool} and~\ref{fig:hyb-jnu-sscCool} the
evolution \addnew{and the emissivity, respectively,} of a \ac{hd} of
electrons with the above specifications subject \rplc{the}{both}
\ac{ssc} \rplc{and their emissivity}{synchrotron cooling} are
shown. \addnew{Differently from the previous case in which the size of
  the system did not make any observable difference, in the case of a
  \ac{hd}, smaller systems undergo a slowlier evolution since there
  are far less synchrotron photons that can be inverse Compton
  upscattered. As a result we observe
  in}\rplc{Regarding}{} \fref{fig:hyb-n-sscCool}\rplc{, we can
  appreciate the effects of the \ac{ssc} cooling on the distribution
  of particles.}{that for the same evolutionary times the cooling
  break of the \ac{eed} has evolved to smaller values of $\gammap$ in
  the case of larger system sizes (dashed lines
  in \fref{fig:hyb-n-sscCool}).} Furthermore,
in \fref{fig:hyb-jnu-sscCool} we can appreciate why the effects of
$\usscp$ are perceptible, and it is so because the \addnew{\ac{mbs}}
emissivity is larger not only in magnitude but it \rplc{covers a larger
spectrum}{extends over a broader frequency range, where the inverse
Compton cross section is significantly larger}.

\begin{figure}[th]
  \centering
  \includegraphics[width=\textwidth]{chambaFigures/sscCool-dis-hyb.eps}
  \caption[Evolution of a hybrid EED subject to SSC cooling]{Similar to
    \fref{fig:pwl-n-sscCool}, but for a hybrid EED. \maacorr{replace $t\Rightarrow \tcom$}}
\label{fig:hyb-n-sscCool}
\end{figure}

\begin{figure}[th]
  \centering
  \includegraphics[width=\textwidth]{chambaFigures/sscCool-emiss-hyb.eps}
  \caption[Evolution of a hybrid EED subject to SSC cooling]{Similar to
    \fref{fig:pwl-jnu-sscCool}, but for a hybrid EED. \maacorr{replace $t\Rightarrow \tcom$, $j_\nu\Rightarrow
      \jnucom$ and $\nu\Rightarrow \nup$}}
\label{fig:hyb-jnu-sscCool}
\end{figure}


%\subsection{Conclusion}
\subsection{Methodological conclusions about SSC cooling}
\label{sec:ssc-conclusion}

According to the above treatment we can conclude that a large emission
region (\SI{\sim{1e20}}{\centi\meter}), or else a \rplc{more
  radiant}{} source \addnew{with a larger \ac{mbs} emissivity}, is
needed for the \ac{ssc} cooling to contribute significantly to the
radiative cooling. \rplc{What is more, despite the fact that}{Indeed,}
in small regions and for pure nonthermal \ac{eed} \rplc{this effect is
  not distinguishable, its effects come to light}{as set up in the
  previous section, SSC cooling is inefficient and may be
  neglected. However, SSC cooling shapes the \ac{eed} evolution and
  its emissivity} for regions of, for instance, the size of an
\ac{agn} jet filled with thermal and nonthermal particles.

It must be pointed out that, although the frequencies outside the domain
$[\nu_{\min}', \nu_{\max}']$ \maacorr{now it must be: $[\nu_{\min}',
  \nu_{\max}'(\gammap)]$} contribute negligibly to the emissivity,
strictly speaking the integral over frequencies in~\eqref{eq:uSSC} should
be performed over \emph{all} frequencies, therefore a wider range of
observed frequencies must be chosen, since the \ac{ssc} cooling term may
vary for the same system. This is a consequence of the nonlinear nature of
$\usscp$. 
%
\maacorr{I do not understand the previous sentences in light of the
  fact that the emissivity extends from the minimum synchrotron
  emitted frequency to the maximum one or the KN cutoff (after the
  editting I have done). I do not know how
  $[\nu_{\min}', \nu_{\max}']$ where set in these tests, but
  certainly, there are physical bounds for the boundaries of the
  frequency interval. Thus, I do not understand the point of saying
  that the integral should be performed over \emph{all}
  frequencies... actually this is (or, at least, must have been) done,
  but outside of the $[\nu_{\min}', \nu_{\max}'(\gammap)]$ range the
  integral is zero.}
%
Additionally, we can appreciate that the code \chamba\ is capable
of resolving consistently the \ac{mbs} emission.

In the simulations of the following chapters the \ac{ssc} cooling is not
considered since, based on the above results, it results to be negligible
at the length scales of the emission region of the \ac{is} model studied in
the present thesis.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "phd"
%%% End:
