import Synchrotron as sync
import numpy as np
import PypersPlots as pp

# gb = 1.0 / np.sqrt(1.0 - np.power(0.1, 2))
# c = np.logspace(-3.0, 8.0, num=200)
# g = np.logspace(np.log10(gb), 4.0, num=200)
x = np.logspace(-4.0, 2.0, num=200)

pp.latexify(txtwdth=369.0, lw=0.7)
fig,ax = pp.initPlot()

ax.set_yscale('log')
ax.set_xscale('log')

ax.plot(x, sync.Fsync(x), lw=1.0, c='r')
ax.plot(x, sync.asymFsync_low(x), lw=1.0, ls="--", c='r')
ax.plot(x, sync.asymFsync_high(x), lw=1.0, ls=":", c='r')
ax.plot(x, sync.Rsync(x), lw=1.0, c='b')
ax.plot(x, sync.asymRsync_low(x), lw=1.0, ls="--", c='b')
ax.plot(x, sync.asymRsync_high(x), lw=1.0, ls=":", c='b')
ax.axvline(x=0.285812, c='#d62728', lw=1.0)
ax.axvline(x=0.229249, c='#1f77b4', lw=1.0)
ax.plot(0.285812, 0.918012, 'ro')
ax.plot(0.229249, 0.712563, 'bo')

pp.decor(ax, xlim=(1e-4,10.), ylim=(1e-3, 3), ylabel=r"$F(X_{\mathrm{c}}), CS86(X_{\mathrm{c}})$", xlabel=r"$X_{\mathrm{c}}$")

#fig.tight_layout()
pp.printer(fig, 'FandR', savedir='/Users/jesus/lab/LaTesis/mbsFigures/', printEPS=True, printPDF=False, printPGF=False, tight=True)
