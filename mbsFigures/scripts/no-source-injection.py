import matplotlib as mpl
import PypersPlots as pp
import extractor as ext
import os
import numpy as np
from scipy.special import kn

def beta(g):
    return np.sqrt(1. - 1./g**2)

############################################################################
# Power-law distribution
############################################################################
def PowerLaw(g,Q0,g1,g2,q):
    if type(g) == float:
        if g <= g1 or g >= g2:
            return 1e-200
        else:
            return Q0 * np.power(g, -q)
    else:

        pwl = np.piecewise(g,
                           [g<g1, (g>=g1) & (g<=g2), g>g2],
                           [lambda x: 0.0,
                            lambda x: Q0 * np.power(x, -q),
                            lambda x: 0.0])
        
        return pwl

#### PLOTTING ####
pp.latexify(txtwdth=369.0, lw=0.7)#, fscale=1.25, ratio=1.2)
fig, ax = pp.initPlot()

ax.set_xscale('log')
ax.set_yscale('log')

p=2.2
gmin=10
gmax=1e4
gamma=np.logspace(0.,4.,1000)

tplots = []
times = np.array([0.0, 1e-2, 1e-1, 5e-1, 1.]) / gmin
for t in times:
    n =  np.power(gmin, p) * np.power(np.maximum(1.-gamma*t,1e-200), p-2.) * PowerLaw(gamma, 1.0, gmin/(1.+gmin*t), gmax/(1.+gmax*t), p)
    tplot, = ax.plot(gamma/gmin, n, lw=1.0)
    tplots.append(tplot)

ax.legend(tplots, [r"$t'=0.0$",
                   r"$t'=0.01/\nu_0 \gamma_{\min}$",
                   r"$t'=0.1/\nu_0 \gamma_{\min}$",
                   r"$t'=0.5/\nu_0 \gamma_{\min}$",
                   r"$t'=1.0/\nu_0 \gamma_{\min}$"],
          loc='best',
          frameon=False,
          fontsize='medium',
          borderaxespad=1.0,
          labelspacing=0.5
)
    
pp.decor(ax, ylim=(1e-6,1e1), ylabel=r"$n(\gamma, t') / n(\gamma_{\min})$", xlabel=r"$\gamma/\gamma_{\min}$",xlim=(1e-1,1e3))
#fig.tight_layout()

pp.printer(fig, 'no-source_inj', savedir="/Users/jesus/lab/LaTesis/mbsFigures/",printEPS=True,printPDF=False,printPGF=False, tight=True)#,onscreen=True)#
