import numpy as np
import PypersPlots as pyp

def pp(g):
    return np.sqrt(g**2 - 1.0)

def GSS88(nu, g):
#    return np.pi**2 * np.exp(2.0 * nu * (1.0 + g * np.log(pp(g) / (1.0 + g))))/ ( 3.0 * np.sqrt( 1.0 + 2.0 * pp(g)**2 * (1.0 + g * np.log(pp(g) / (1.0 + g) ) ) ) )
    return np.pi**2 * np.exp(2.0 * nu) * ((g - 1.0) / pp(g))**(2.0 * g * nu) / ( 3.0 * np.sqrt( 1.0 + 2.0 * (g**2 - 1.0) * (1.0 + g * np.log((g - 1.0) / pp(g)) ) ) )

nu = np.logspace(-1.0, 2.0, num=200)

pyp.latexify(txtwdth=369.0/1.5, lw=0.7, landscape=False)
fig, ax = pyp.initPlot()

ax.set_yscale('log')
ax.set_xscale('log')

ax.axvspan(10.0, 100.0, facecolor='0.8')
ax.plot(nu, GSS88(nu, 1.05), lw=1.0)
ax.plot(nu, GSS88(nu, 1.25), lw=1.0)
ax.plot(nu, GSS88(nu, 1.5), lw=1.0)
ax.plot(nu, GSS88(nu, 2.0), lw=1.0)


pyp.decor(ax, xlim=(0.1, 100.0), ylim=(1e-3, 10.0), ylabel=r"$P_{\nu'}'(\gamma) c / \nu_{\mathrm{g}} e^2$", xlabel=r"$\nu' / \nu_{\mathrm{g}}$")

#fig.tight_layout()
pyp.printer(fig, 'GGS88', savedir='/Users/jesus/lab/LaTesis/mbsFigures/', printEPS=True, printPDF=False, printPGF=False, tight=True)
