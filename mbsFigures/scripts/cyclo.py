import numpy as np
import PypersPlots as pp
import Synchrotron as syn

m = np.arange(1,5)

pp.latexify(txtwdth=369.0, lw=0.7)
fig, ax = pp.initPlot()

ax.set_yscale('log')

b = 0.1
PT = syn.totCyclo(10,b)
Pm = syn.Cyclo(m,b)/PT
p1, = ax.plot(m,Pm, 'ro')
for mm in m:
    ax.plot((mm, mm), (1e-6, syn.Cyclo(mm,b)/PT), 'r-', lw=1.0)
b = 0.05
PT = syn.totCyclo(10,b)
Pm = syn.Cyclo(m,b)/PT
p2, = ax.plot(m,Pm, 'bs', ms=4.0)
for mm in m:
    ax.plot((mm, mm), (1e-6, syn.Cyclo(mm,b)/PT), 'b-', lw=1.0)

ax.legend((p1, p2),
          (r"$\beta = 0.1$",
           r"$\beta = 0.05$"),
          loc='best',
          fontsize='large',
          frameon=False,
          borderaxespad=2.0,
          labelspacing=0.8
)

pp.decor(ax,
         xlim=(0.0, 5.0),
         ylim=(1e-6, 3.0),
         ylabel=r"$P_{m}' / {P'}_{\hspace{-0.1cm}\mathrm{T}}$",
         xlabel=r"$m$" ,
         labels_kw={'fontsize' : 'x-large'},
         ticks_kw={'labelsize' : 'large'}
)
ax.tick_params(axis='x',which='minor',bottom='off',top='off')

#fig.tight_layout()
pp.printer(fig, 'EmissLines', savedir='/Users/jesus/lab/LaTesis/mbsFigures/', printEPS=True, printPDF=False, printPGF=False, tight=True)

