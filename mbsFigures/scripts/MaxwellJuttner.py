import matplotlib as mpl
import PypersPlots as pp
import extractor as ext
import os
import numpy as np
from scipy.special import kn

def beta(g):
    return np.sqrt(1. - 1./g**2)

############################################################################
# Maxwell-J\"{u}ttner distribution
############################################################################
def n(g,T):
    return np.maximum( 1e-200,
        g**2 * beta(g) * np.exp(-g / T) / (T * kn(2, 1. / T))
    )

#####  PLOTTING  #####
#mpl.rcParams.update({"figure.autolayout": True})
pp.latexify(txtwdth=369.88582)
fig, ax = pp.initPlot()
gamma = np.logspace(0., 0.6, 100)
th1, = ax.loglog(gamma, n(gamma, 0.1), 'k')
gamma = np.logspace(0., 1.4, 100)
th2, = ax.loglog(gamma, n(gamma, 1.), 'r')
gamma = np.logspace(0., 2.2, 150)
th3, = ax.loglog(gamma, n(gamma, 5.), 'b')
gamma = np.logspace(0., 2.4, 150)
th4, = ax.loglog(gamma, n(gamma, 10.), 'g')
gamma = np.logspace(0., 3.3, 200)
th5, = ax.loglog(gamma, n(gamma, 100.), 'm')

ax.legend((th1, th2, th3, th4, th5),
          (r"$\Theta_{\mathrm{e}} = 0.1$",
           r"$\Theta_{\mathrm{e}} = 1$",
           r"$\Theta_{\mathrm{e}} = 5$",
           r"$\Theta_{\mathrm{e}} = 10$",
           r"$\Theta_{\mathrm{e}} = 100$"),
          loc='best',
          fontsize='medium',
          handlelength=1.5,
          labelspacing=0.3
)

yminor_ticks = []
for k in range(-7,1):
    for i in range(2,10):
        yminor_ticks.append(i*10**k)
ax.yaxis.set_ticks(np.logspace(-7.0, 1.0, num=9))
ax.yaxis.set_ticks(yminor_ticks, minor=True)


pp.decor(ax, ylim=(1e-7,1e1), ylabel=r"$n_{\mathrm{th}}(\gamma)$", xlabel=r"$\gamma$",xlim=(0.7,3e3))
pp.printer(fig, 'MaxwellJuttner', savedir="/Users/jesus/lab/LaTesis/mbsFigures/",printEPS=True,printPDF=False,printPGF=False)#, tight=True)#,onscreen=True)#
