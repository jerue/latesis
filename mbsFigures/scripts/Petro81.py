import numpy as np
import PypersPlots as pyp

def bofg(g):
    return np.sqrt(1.0 - 1.0 / g**2)

def cot(x):
    return 1.0 / np.tan(x)

def tt(g,th):
    return bofg(g) * g * np.sin(th)

def Z(g, th):
    return tt(g, th) * np.exp(1.0 / np.sqrt(1.0 + tt(g, th)**2)) / ( 1.0 + np.sqrt(1.0 + tt(g, th)**2))

def mm(g, th, nu):
    return nu * (1.0 + tt(g, th)**2) / g

def P81(nu, g, th):
    return np.sqrt(np.pi * nu) * ((1.0 + 2.0 * (cot(th) / g)**2) * (1.0 - (bofg(g) * np.cos(th))**2)**(0.25)) * Z(g, th)**(2.0 * mm(g, th, nu)) / g

nu = np.logspace(-2.0, 2.0, num=200)

pyp.latexify(txtwdth=369.0/1.5, lw=0.7, landscape=False)
fig, ax = pyp.initPlot()

ax.set_yscale('log')
ax.set_xscale('log')

ax.axvspan(10.0, 100.0, facecolor='0.8')
ax.plot(nu, P81(nu, 1.05, 0.5*np.pi), lw=1.0)
ax.plot(nu, P81(nu, 1.25, 0.5*np.pi), lw=1.0)
ax.plot(nu, P81(nu, 1.5, 0.5*np.pi), lw=1.0)
ax.plot(nu, P81(nu, 2.0, 0.5*np.pi), lw=1.0)

xminor_ticks = []
for k in range(-2,3):
    for i in range(2,10):
       xminor_ticks.append(i*10**k)
ax.xaxis.set_ticks(np.logspace(-2., 2., num=5))
ax.xaxis.set_ticks(xminor_ticks, minor=True)

pyp.decor(ax, xlim=(0.01, 100), ylim=(1e-3, 2.0), ylabel=r"$P_{\nu'}'(\gamma, \vartheta') c / \nu_{\mathrm{g}} e^2$", xlabel=r"$\nu' / \nu_{\mathrm{g}}$")

#fig.tight_layout()

pyp.printer(fig, 'P81', savedir='/Users/jesus/lab/LaTesis/mbsFigures/', printEPS=True, printPDF=False, printPGF=False, tight=True)
