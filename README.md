# *LaTesis* README


## Compiling the thesis

```shell
$ ./2pdf
```


## DRAFT mode

The draft mode can be switched on/off in the 4-th line of `phd.tex`:

``` tex
%\RequirePackage[l2tabu, orthodox]{nag}
\RequirePackage{ifthen}
\newboolean{isDraft}
\setboolean{isDraft}{true}     % true for memoir draft mode
```


### DRAFT mode OFF

When draft mode is off, the generated PDF will have the final dimensions of
the thesis.


### DRAFT mode ON

The draft mode will generate an A4 PDF. The following block defines and
sets boolean variables:

``` tex
\newboolean{withLinu}
\newboolean{withCrop}
\newboolean{withFigs}
\newboolean{withWamk}
\setboolean{withLinu}{false} %%% true for numbered lines 
\setboolean{withCrop}{false} %%% true for trim marks
\setboolean{withFigs}{false} %%% true for hiding figures
\setboolean{withWamk}{false} %%% true for DRAFT watermark
```

The function of each variable is:

* **Numbered lines**: if set to true lines will appear numbered in the PDF.
* **Trim marks**: if set to true trim marks will appear showing the thesis
  dimensions within an A4 paper size.
* **Figures**: if set to false a frame and the address of the figure will
  appear in place of the figure. This is specially useful when focoused on
  writting since it speeds up compilation.
* **Watermark text**: if set to true the watermark text **Draft** will
  appear in the background.


## Chapter by chapter revision

For fast compilation and handling I have created a chapter standalone file
(`chap.tex`). This will help to avoid changes in the main `phd.tex` file
and not only but also for creating single chapters for locals revisions.

To make sure that changes are untracked for this file run:

``` shell
$ git update-index --skip-worktree chap.tex
```

After this `chap.tex` can be modified and, since it is listed in the
`.gitignore`, it will be obviated when doing commits.

In this file any number of chapters can be included (and in whichever
order) to your liking just by adding `\include{<texchaterpname>}`.


### Compiling `chap.tex`

``` shell
$ ./2pdfchap
```

## A note on switching DRAFT mode on/off

To generate the proper .aux files one should run at least once the `2pdf`
or `2pdfchap` command.


### A note on macro `\autoref`

If there is an issue with `\autoref` at some point it when using the DRAFT
mode is because this mode does not load hyperref. To avoid this issue do the
following changes in case I have not done yet:

    \autoref{fig: --> \fref{fig:
    \autoref{tab: --> \tref{tab:


## A note on TODO notes

The notes and the notes manual are controlled by the following block of
code:

``` tex
\todototoc%
\listoftodos[To Do List]
\ifdraftdoc{}
  \include{notes}
\fi
```

It will be commented out in the `phd.tex` and uncommented in the `chap.tex`
to avoid different versions of `phd.tex`, but feel free to uncommentit it
if you need to.

