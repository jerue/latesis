\chapter{Introduction}
\label{ch:intro}

% \epigraphhead{
% \epigraph{\footnotesize
%   \begin{tabular}{ll}
%     To the quasar & To the dark star\\
%     Beyond the milky way & Beyond the light of day\\
%     The red shift is high, & The gravity's high,\\
%     a bright light in the sky & a black sun in the sky\\
%     & 3C273 \\
%   \end{tabular}
% }{\textit{To The Quasar}\\ \textsf{Ayreon}}
% }
%\noindent
In the present chapter a brief history and state-of-the-art
observations and theoretical models of blazars are described.

\section{Active Galactic Nuclei}

An \ac{agn} is a region of a galaxy that is brighter than normal, typically
associated with the presence of a \ac{smbh} in the galactic center. Its
luminosity can reach values \SI{\sim{}1d45}{\erg\per\second}. The
observations of these objects start in the early XX century when
astronomers began to realize that the nuclei of some galaxies had optical
emission lines. The first systematic study of the optical emission from
these objects was performed by \citet{Seyfert:1943aj}. A few years earlier
\citet{Jansky:1932cp} discovered a strong radio signal, which he thought to
be associated with the Sun, albeit later he concluded that the emission
came from the center of the Milky Way \citep{Jansky:1935fg}. These studies
are considered a breakthrough for the development of radio astronomy, which
saw great progress in the following years.  During these years the
identification and measurement of the structure of radio sources also made
large advances not only in radio but also in optical, e.g.:
\emph{Vir A} (\emph{M87}), and \emph{Cyg A} \citep{Shields:1999br}.
\citet{Jennison:1953dg} discovered that the structure of Cyg A was best
modeled as a two-component source, both components having approximately the
same intensity (see \fref{fig:cygA}). A short time later a jet structure
emanating from the nucleus of \emph{M87} was observed by
\citet{Baade:1954mi}. A decade later \citet{Schmidt:1963qs} discovered the
first \ac{qso}: \emph{3C 273}.
%
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{introFigures/CygA.eps}
  \caption[Cygnus A at 5 GHz]{\emph{Cygnus A} observed by the VLA at
    \SI{5}{\giga\hertz} (Image courtesy of NRAO/AUI).}\label{fig:cygA}
\end{figure}

A high variability of these objects and their large redshifts have been
measured \citep{Matthews:1963sa,Greenstein:1964sc,Schmidt:1964ma}, proving
them to be active, extragalactic and being hosted by a galaxy. However, the
theoretical foundation of the physics responsible for this emission was not
clear. \citet{Alfven:1950rs} proposed the synchrotron process as
responsible for the radio emission coming from the Sun and \emph{radio
  stars}. With this idea \citet{Kiepenheuer:1950vr} tried to explain the
Galactic radio background as synchrotron radiation coming from
ultrarelativistic electrons accelerated by the interstellar magnetic
fields, taking into account relativistic corrections to the emitted
frequency, and later supported by the corrections\footnote{The calculations
  by \citet{Kiepenheuer:1950vr} carried an error by using the synchrotron
  approximation for frequencies $\nu \ll \nuc$ (see \Sref{sec:synchrotron})
  for the case of $\nu > \nuc$} by \citet{Ginzburg:1951cr}. Moreover,
\citet{Burbidge:1956sm} estimated that in order to explain the high
luminosities in the optical and radio coming from \emph{M87}, the energy
supplied to the electrons in the jet, assuming that they are moving in a
magnetic field $\lesssim \SI{1e-2}{\gauss}$, had to be
$\gtrsim \SI{1e49}{\erg}$. Furthermore, \citet{DeYoung:1967ax} pointed out
that the estimated strength of the intergalactic magnetic field would not
be enough to collimate the material ejected from the nucleus of, e.g.,
\emph{Cyg A}. Disregarding other possible relativistic effects, they
proposed that the jet structure is more likely dynamic rather than in
hydrostatic equilibrium with the intergalactic medium and bering its own
magnetic field. This was done by balancing the internal pressure of the jet
material with the ram pressure of the intergalactic medium and estimating
that the mass of the jet is $\sim 2\times 10^{6} M_{\odot}$, the density of
the jet material $\gtrsim$ \SI{1e-30}{\gram\per\cubic\centi\meter} and the
time required to stop it $\lesssim$ \SI{2e5}{\year}, this being comparable
to the actual age of \emph{Cyg A}.

Until that point there was no preferred candidate to explain the nature of
the central object of these kind of radio sources. Reasoning from first
principles \citet{Lynden:1969be} proposed that the objects in the nuclei
of, e.g., \emph{M87}, \emph{Cyg A} or \emph{Sgr A}, result from the
collapse of old quasars with a mass $\sim 10^{9} M_{\odot}$, and that a
flat disk of gas encompasses these objects. He was the first to propose a
model containing both a \ac{smbh} and an \ac{ad}. After several decades and
many observations in the whole electromagnetic spectrum, astrophysicists
proposed the following scenario for \acp{agn}, which has been so far the
most accepted one \citep{Urry:1995aa}: A \ac{smbh} is surrounded by a
luminous \ac{ad}. Close to the \ac{ad} the clouds producing broad emission
lines (\ac{blr}) orbit, and further out there is a thick \ac{td} (or
possibly a warped disk). Above the \ac{ad} there is a hot corona. Finally,
the radio jets are launched from a region close to the \ac{bh}, and farther
above, surrounding the jet, there are clouds which produce the narrow
emission lines, called the \ac{nlr} (see \fref{fig:agn-scheme}).
%
\begin{figure}[ht]
  \centering
  \includegraphics[width=0.5\textwidth]{introFigures/agnmodel.eps}
  \caption[AGNs outline]{\acp{agn} outline (not in scale). Credit:
    P. Padovani}.
\label{fig:agn-scheme}
\end{figure}


\subsection{AGNs zoo}
\label{sec:agns-zoo}

The \ac{agn} classification is a challenging topic. The classes of
\acp{agn} are multivariate, i.e.  they depend not only on the observed
morphology but also on spectral characteristics and detection criteria
\citep[e.g.,][]{Urry:1995aa,Tadhunter:2008an,Dermer:2016jc}. The first
division is made according to their radio loudness; i.e., their radio to
optical flux ratio
%
\begin{equation}
  \label{eq:rad-opt-rat}
  \dfrac{F_{5}}{F_{B}} \gtrsim 10,
\end{equation}
%
where $F_{5}$ and $F_{B}$ are the radiation flux at \SI{5}{\giga\hertz} and
at the \emph{B}-band (\SI{445}{\nano\meter}). In the optical, \acp{agn} can
be classified by the brightness and width of their emission lines: broad
and bright or narrow and weak.

Along the history of the understanding of \acp{agn}, their taxonomy,
divisions and unification have undergone several changes depending on the
observational technique used, orientation or morphology of the objects
\citep[see][for a deep review on the consensus of \acp{agn}
classification]{Urry:1995aa,Tadhunter:2008an}. However, state of the art
observations \citep[e.g.][]{Husemann:2016pt} have shown that \acp{agn} may
change of type in time and therefore their spectral features are not
necessarily orientation or morphology-wise dependent but maybe other kind
of phenomena (e.g., \acp{tde}), have to be considered. Here we will mention
the families the author found exemplary for the context of the present
work:
%
\begin{flexlabelled}{bfseries}{0pt}{*}{*}{*}{*}
\item[Seyfert 1] Spiral galaxies with broad permitted and
  forbidden\footnote{The \emph{forbidden lines} are the spectral lines
    corresponding to the absorption or emission associated to the less
    likely transitions of the electrons in an atom.} emission lines
\item[Seyfert 2] Spiral galaxies with narrow forbidden lines.
\item[NLRG] \Acp{nlrg} are radio loud galaxies with narrow emission
  lines. This kind of galaxies includes the Fanaroff-Riley radio galaxies
  \citep{Fanaroff:1974ri}:
  \begin{flexlabelled}{bfseries}{0pt}{*}{*}{*}{*}
  \item[FR I] The hotspots (brightest regions) are closer to the nucleus of
    the galaxy and the source becomes fainter as one approaches the outer
    region. The jets of these galaxies often appear symmetric in radio.
  \item[FR II] The hotspots are farther from the nucleus of the
    galaxy. Their morphology consists in more collimated jets and well
    recognizable radio lobes.
  \end{flexlabelled}
\item[FSRQ] \Acp{fsrq} are radio loud galaxies with broad emission lines.
\end{flexlabelled}

There is a third species of \acp{agn}, which shows extremely variable
spectrum (see \fref{fig:multiwave-lcs}). What characterizes these objects
is their small angle to the line of sight of the observer. If the object is
radio quiet the object is called a \ac{bal}. Otherwise we are facing a
\emph{blazar}\footnote{The name \emph{blazar} was coined by the astronomer
  Edward Spiegel in 1978 to designate those objects with strong nonthermal
  broadband continuum and narrow emission lines.}.
%
\begin{SCfigure}%[ht]
  \centering
  \includegraphics[width=0.65\textwidth]{introFigures/Marscher00.eps}
  \caption[Multiwavelength light curves.]{Multiwavelength light curves in
    $\gamma$-rays, optical R band, X-rays and radio. Credits
    \citet{Marscher:2010jo}.}
\label{fig:multiwave-lcs}
\end{SCfigure}


\section{Blazars}
\label{sec:blazar-intro}

Blazars are a kind of highly variable and radio loud \acp{agn} whose
features are best explained by nonthermal radiation from a relativistic jet
closely aligned to the line of sight of the observer
\citep[e.g.][]{Blandford:1974re}. The relativistic plasma moves along the
jet, which is the channel along which the central engine supplies momentum
and energy to the extended radio structures, reaching distances of
$\SIrange{0.1}{1}{\mega\parsec}$ from the nucleus \citep{Urry:1995aa}. The
broadband continuum of blazars goes from radio frequencies to
$\gamma$-rays, developing a double bump in their \ac{sed} (see
below). Blazars typically represent less than 5\% of all \acp{agn}
\citep{Ajello:2009co,Ajello:2012al}
%
\begin{figure}[ht]
  \centering
  \includegraphics[width=0.999\textwidth]{introFigures/agn_spectra.eps}
  \caption[Broadband emission of blazars]{Broadband emission of
    blazars. Credit: A.E. Wehrle/M.A. Catanese/J.H. Buckley/Whipple
    Collaboration.}
\label{fig:seds-blazars}
\end{figure}

These objects are named after \emph{BL Lacertae} (see
\fref{fig:BLLac-pos}), first observed by \citet{Hoffmeister:1929an} showing
high variability emission lines, but at that time it was categorized as an
\emph{irregular variable star} in the \emph{Milky Way}. Later
\citet{Schmitt:1968nt} identified \emph{BL Lac} with the radio source
\emph{VRO} 42.22.01, finding its radio spectrum to be polarized. A decade
later \citet{Oke:1974al} observed that the spectra coming from it showed
absorption lines typical of an ordinary giant galaxy with redshift
$z = 0.07$.
%
\begin{SCfigure}
  \centering
  \includegraphics[width=0.7\textwidth]{introFigures/BLLac_pos.eps}
  \caption[BL Lac in the optical band]{\emph{BL Lacertae} in the optical
    band (red). This figure was made using the \emph{Aladin sky atlas} web
    applet \citep{Bonnarel:2000at,Boch:2014ww}, using the second Digitized
    Sky Survey (DSS2).}
\label{fig:BLLac-pos}
\end{SCfigure}

Blazars are commonly classified according to the relative strength of their
observed spectral components. Each component is associated to the
contribution of a relativistic jet (nonthermal emission), to the \ac{ad}
and to the \ac{blr} (thermal radiation), and to the light from the host,
usually a giant elliptical galaxy \citep{Urry:2000sc}. The broadest
component of the spectrum is the nonthermal one, and it spans the whole
electromagnetic frequency range, usually displaying two broad peaks or
``humps'' (see \fref{fig:seds-blazars}). The lower-frequency peak ranges
between \SIrange{1e12}{1e17}{\hertz} (radio and X-rays), while the
high-frequency spans from X-rays to $\gamma$-rays, and is believed that is
caused by the \ac{ic} scattering
\citep{Jones:1974od,Hoyle:1966bu,Fossati:1998ay}.

Depending on the features displayed by their emission lines, blazars can be
further subdivided into \citep{Sambruna:1996ma}:
%
\begin{flexlabelled}{bfseries}{0pt}{*}{*}{*}{*}
\item[BL Lac] \acp{bllac} have featureless emission lines or show only weak
  absorption lines. Their synchrotron peak frequency ranges between radio
  and X-rays.
\item[FSRQ] \acp{fsrq} are characterized by broad emission lines. Their
  synchrotron peak is found between radio an near infrared.
\end{flexlabelled}
%
Moreover, depending on the peak frequency of their \acp{sed} synchrotron
component ($\nusynobs$), blazars have been recently further subclassified
into \ac{lsp}, \ac{isp} and \ac{hsp} \citep[e.g.][]{Finke:2013aj}. In the
former group we can find \acp{lbl} and most \acp{fsrq} with $\nusynobs$
\SI{<1e14}{\hertz}, in the intermediate group we find \acp{ibl} few
\acp{fsrq} with $\nusynobs$ \SIrange{\approx1e14}{1e15}{\hertz}, and in the
latter group we find \acp{hbl} with $\nusynobs$ \SI{>1e15}{\hertz}
\citep{Sambruna:1996ma,Abdo:2010ac,Giommi:2012aa}.


\subsection{Blazar models}%
\label{sec:blazar-models}

As we have mentioned above, there is a broad consensus that the low
frequency peak is due to the synchrotron emission from relativistic
electrons gyrating in a magnetic field. As for the high frequency peak, as
mentioned before, it may be the result of \ac{ic} scattering. To account
for observations, different ideas have been put forward about where the
seed photons for \ac{ic} scattering come from. For instance: \ac{ic}
synchrotron photons intrinsic to the jet result in \ac{ssc} emission
\citep{Bloom:1996ma}, upscattering of soft photons from a blackbody \ac{ad}
\citep{Dermer:1993hr}, upscattering of photons initially from an \ac{ad}
and then scattered by the \ac{blr} \citep{Sikora:1994aj}, upscattering of
photons coming from the \ac{td} \citep{Kataoka:1990ma} are all examples of
\ac{eic} emission.

So far it is unknown where in the jet all the aforementioned emission
processes take place. There are several points of view on this regard. For
instance, there was a proposal by \citet{Blandford:1979ko} saying that the
radiation comes from inhomogeneities in the jet such as accelerated blobs
of plasma or shock waves that travel along the jet. This model belongs to
the so called \emph{multi-zone} models. There are others in this group like
the \emph{conical standing shock} \citep{Jorstad:2004ma,Marscher:2008jo},
which proposes that the bright, compact, quasi-stationary features in the
innermost regions of the jet revealed by \emph{VLBA}\footnote{Very Long
  Baseline Array is a system of radio antennas located in New Mexico, USA.}
and \emph{VLBI}\footnote{\emph{Very-long-baseline interferometry} is a
  technique used in astronomy for imaging radio sources.} observations
\citep{Jorstad:2005aj,Fromm:2013ro} is accounted for by conical standing
shocks; these shocks recollimate the ejected material at ultrarelativistic
speeds, producing the $\gamma$-ray flares. Also the \emph{turbulent extreme
  multi-zone} model, introduced by \citet{Marscher:2014te}, considers not
only ultrarelativistic material crossing the recollimation shock but also
turbulent plasma, accounting for the polarization features in the radio
``cores''. Another model is the \emph{spine-layer} which proposes a jet
with subrelativistic external flow surrounding a much faster inner flow
\citep{Henri:1991pe,Ghisellini:2005ta}.

On the other hand, the observation of ``knots'' or blobs in jets of
\acp{agn} has led to propose \emph{one-zone} models for blazars, which
attempt to account for flare emission in the observed spectra
\citep{Rees:1978km}. The most popular, and yet most successful up to date,
is the \emph{shock-in-jet} model in which it is assumed that instabilities
in the jet, or the intermittent ejection of plasma by the central engine,
produce \acp{is} at some point along the jet
\citep{Marscher:1985am,Spada:2001do}.


\subsection{The blazar zone}%
\label{sec:blazar-region}

The \emph{blazar zone} is where most of the kinetic energy of the jet is
transformed into radiation and where it is assumed that particles are being
accelerated \citep[e.g.][]{Bykov:2012ge}. The size of the \emph{blazar
  zone} has been estimated to be of \SIrange{1e15}{1e17}{\centi\meter}
\citep{Georganopoulos:2001ki}.  Some models suggested that $\gamma$-rays
are produced within the \ac{blr} and that the $\gamma$-ray emitting radius
ranges roughly between \SIrange{0.03}{0.3}{\parsec} \citep[][see
\fref{fig:blazar-region}]{Ghisellini:1996ma}. It has also been argued that
in relativistic jets of powerful blazars the $\gamma$-rays emission regions
are within cavities formed by the \ac{blr}. However, it has also been
observed that the emission region is outside the \ac{blr}
\citep[e.g.][]{Agudo:2011jo}.
%
\begin{SCfigure}%[ht]
  \centering
  \includegraphics[width=0.6\textwidth]{introFigures/blazar_region.eps}
  \caption[Schematic view of the blazar zone]{Schematic view of the blazar
    zone for the model where high energy emission occurs in the \ac{blr}.}
\label{fig:blazar-region}
\end{SCfigure}

Regarding the particle population of the radiating material in jet there
are two main contending models: leptonic and hadronic. In the leptonic
model the high-energy emission is produced by relativistic electrons which
are accelerated through shocks or magnetic reconnection that \ac{ic}
upscatter both low frequency photons from the external medium (\ac{eic})
and synchrotron photons produced in the jet (\ac{ssc}). The hadronic model
assumes that the relativistic protons (that may be present in the jet
besides electrons) are able to produce the high energy emission via
proton-synchrotron radiation (for which strong magnetic fields are
required) and energy losses due to photomeson production and pair
production \citep{Begelman:1990ru,Mannheim:1993pr,Mucke:2003bb}

So far we have not discussed the fact that the magnetic fields are
ubiquitous in \acp{agn}. In fact, little is known about the true role that
the magnetic field play in the emission of blazars since, depending on the
model and the distance from the central engine, the magnetic field has a
different influence on the phenomena in the jet; for instance, the magnetic
field strength determines the efficiency of electron acceleration at
shocks. Nevertheless, detailed studies of the magnetic field strength and
geometry have proved useful for approaching to resolve this issue
\citep[e.g.][]{Porth:2011fe,Mimica:2012aa,Janiak:2015si}.


\subsection{The blazar sequence}
\label{sec:agn-sequence}

In an attempt to unify blazars there are hints suggesting that there is an
evolutionary trend among these sources. The \ac{sed} peaks of high
luminosity sources shift to higher frequencies in low luminosity sources;
i.e., there seems to be an evolutionary trend from \acp{fsrq} then to
\acp{lbl} and finally towards \acp{hbl}
\citep{Fossati:1998ay,Bottcher:2002pd}. With the aim of clarifying this
trend a new parameter was introduced by \citet{Finke:2013aj} which is the
\emph{Compton dominance}:
%
\begin{equation}
  \label{eq:comtp-dom}
  A_C := \dfrac{\LL_\IC}{\LL_\syn},
\end{equation}
%
where $\LL_\IC$ and $\LL_\syn$ are the luminosities of the \ac{ic} and
synchrotron components of the \ac{sed}. It has been observed that in the
$A_C\text{-}\nusynobs$ plane there is an ``L''-shaped transition from
\acp{fsrq} to \acp{bllac} showing the luminosity decrease of the \ac{ic}
component (see \fref{fig:comp-dom-intro}).
%
\begin{SCfigure}%[h]
  \centering
  \includegraphics[width=0.65\textwidth]{introFigures/Finke2013.eps}
  \caption[Compton dominance]{\emph{Compton dominance} versus synchrotron
    peak frequency. Credit: \citet{Finke:2013aj}.}
\label{fig:comp-dom-intro}
\end{SCfigure}

By modelling blazar \acp{sed} \citet{Ghisellini:1998hg} found that this
sequence can be accounted for by the increasing role of the external photon
field in the cooling factor of relativistic electrons in the jet, since the
time evolution of the electrons Lorentz factor, $\gamma$, is governed by
(see \Sref{sec:part-evol}):
%
\begin{equation}
  \label{eq:el-cool-fact}
  \dot{\gamma} = - \dfrac{4}{3} c \sigT \dfrac{\uB + u_{\ext}}{\mel c^{2}}
  \gamma^{2}
\end{equation}
%
where $\uB$ and $u_{\ext}$ are the magnetic and external photon field
energy densities. Based on this, models have been proposed where $u_{\ext}$
takes a range of values, or is given as a function of the distance to the
central engine, rather than having a constant value
\citep{Ghisellini:2009mnras,Sikora:2009co,Saito:2015st}.


\section{The internal shocks model}
\label{sec:int-shock-intro}

Shock waves are ubiquitous in nature. In astrophysics they are typically
regarded as regions where particles are accelerated and important radiation
processes take place. In \acp{agn}, particularly in blazars, there has been
some evidence that shock waves appear in the jet. Their origin may be due
to either instabilities in the jet flow, arising from the jet/ambient
interaction or from the magnetic field topology, or to the non-linear
evolution of an inhomogeneous or time-variable jet generation
\citep[e.g.][]{Begelman:1990ki,Baring:2012RJAGN}. As mentioned above, the
\ac{is} (or shock-in-jet) model had an important role in the understanding
of the blazar spectra and has been successful in explaining many of the
features of the blazar variability and flares.

The internal shocks model for blazars considered in this work rests on the
idea that that a central engine ejects shells of plasma with different
mass, energy and velocity. This translates into the presence of relative
motions in the relativistic jet. Early blobs will decelerate (cool down,
with bulk Lorentz factor $\GammaR$) as they move further away from the
central engine \citep[e.g.][]{Rees:1994ca}. Faster shells (with bulk
Lorentz factor $\GammaL$) will catch-up with the slow ones at a time
$t_{collision}$ producing ``collisions'' (see
\fref{fig:int-shocks-rep}). During the collision process the plasma is
shocked, dissipating energy in the form of acceleration of particles, and
in turn as radiation. If magnetic fields are dynamically negligible, two
shocks form separated by a contact discontinuity (CD): one propagating into
the slower shell (forward shock; FS) and another one slowing down the
faster shell (reverse shock; RS).
%
\input{introFigures/intshocks}

This model was first proposed almost thirty years ago by
\citet{Rees:1978km} accounting for the optical knots in the \emph{M 87} jet,
and later by \citet{Marscher:1985am}. It was not until the beginning of the
past decade that the \ac{is} model was revived
\citep[e.g.][]{Spada:2001do,Bicknell:2002is,Mimica:2004ay}, and since then
this scenario has been thoroughly explored using analytic and (simplified)
numerical modelling
\citep{Kobayashi:1997vf,Daigne:1998wq,Spada:2001do,Bosnjak:2009dv,Daigne:2011aa}
and by means of numerical hydrodynamics simulations
\citep{Kino:2004in,Mimica:2004ay,Mimica:2005aa,Mimica:2007aa}.

More recently, the effects of strong magnetic fields on the shell
collisions have been taken into account. Given the fact that synchrotron
emission from the accelerated particles in the shocked plasma fits
considerably well the observations, the emitting plasma must be magnetized,
to some extent. However, the degree of magnetization of the jet flow has
not yet been determined, and whether the radiation we observe results from
the dissipation of its magnetic energy in addition to its kinetic energy is
not known, either. In the case of moderate or strong magnetic fields the IS
scenario has to be modified to account for the differences in the dynamics
\citep[e.g., the suppression of one of the two shocks resulting in a binary
collision][]{Fan:2004gt,Mimica:2010aa} and the emission properties of the
flares \citep{Mimica:2007aa,Mimica:2012aa,Rueda:2014mn,Rueda:2017hd}.


\subsection{Acceleration efficiency}
\label{sec:accel-effic-intro}

The \ac{is} model assumes that a fraction $\epse$ of the shock power is
transferred to the charged particles and, by means of some acceleration
process (e.g.  first-order Fermi process; \citealt{Fermi:1954gm}), these
particles are injected into the unshocked material at the shock front. Even
in the case in which a plasma is very weakly magnetized (i.e., microscopic
magnetic fields tight the particles constituting the plasma, which is
assumed to be collisionless), stochastic magnetic fields will be produced
\emph{in situ} by shocks through, e.g., the Weibel instability
\citep{Weibel:1959,Medvedev:1999,Gruzinov:1999}, converting the free energy
of counter-streaming flows into small-scale (skin-depth) magnetic
fields. For practical purposes, it is commonly assumed that another
fraction $\epsB$ of the internal energy density of the plasma is
transformed into a magnetic field, whose orientation in space is expected
to be random \citep[e.g.][]{Joshi:2011bp,Mimica:2012aa}.

The values of $\epse$ and $\epsB$ are up to date unknown. Although,
advances have been made on the expected values, both from the observational
data \citep[see][]{Santana:2014ba,Kumar:2015zh}, as well as from \ac{pic}
simulations
\citep[e.g.][]{Nishikawa:2003,Hededal:2004,Nishikawa:2005,Sironi:2009aa,Sironi:2013hf}. 
Moreover, lower boundaries to the fraction of dissipated power have been
estimated by \citet{Mimica:2010aa} through numerical \ac{mhd} modeling.


\subsection{Distribution of particles}
\label{sec:distr-part-intro}

From the observed synchrotron spectra of blazars it has been shown that
they fit a power-law at its highest frequencies \citep[between optical and
X-rays, e.g.][]{Ginzburg:1965gc,Fossati:1998ay}. This evidence tells us
that somewhere in a blazar particles are being accelerated in such a way
that they follow a power-law in energies \citep{Rybicki:1979}, or
equivalently in Lorentz factors $\gamma$, with a cutoff at a maximum value
$\gmax$. Ever since this idea was proposed by \citet{Ginzburg:1963me}, a
pure power-law distribution has been considered for the theoretical
modeling of blazars. In order to comply with the relativistic regime in
which the synchrotron radiation is emitted, the Lorentz factor distribution
of the emitting electrons is customary lower-bounded at a suitably
prescribed minimum Lorentz factor $\gmin$ (see \Sref{sec:non-therm-part}).

In other astrophysical scenarios this spectrum profile (power-law) has been
observed as an extension or ``tail'' of a thermal distribution, e.g.~:
neutron stars, accretion disks, supenovae, \acp{grb}
\citep[e.g.][]{Ghisellini:1988nw,Li:1996pf,Ozel:2000tr,Peer:2009ca}. Moreover,
\ac{pic} simulations have shown that in relativistic shocks a ``hybrid''
thermal-nonthermal distribution is naturally produced in shocked plasma
models. These kind of distributions range velocities from subrelativistic
to ultrarelativistic
\citep[see][]{Spitkovsky:2008fe,Sironi:2009aa,Sironi:2013hf}.

Despite the fact that there is evidence of \acp{hd} in relativistic
astrophysical scenarios, this kind of distributions have not been deeply
studied in the \ac{is} model of blazars.


\section{Numerical treatment of the MBS emission}
\label{sec:charg-part-magn}

It has been observed in the spectra of \acp{grb} and \acp{agn} that
magnetic fields in their jets play a very important role by their radiative
signature. It has been typically assumed that electrons in collisionless
shocks are efficiently accelerated until they reach ultrarelativistic
energies. Hence, their emission is properly computed as a result of the
synchrotron process. However, as we have pointed out in
\Sref{sec:distr-part-intro}, it is not unlikely that the distribution of
shock-accelerated electrons extends towards the moderately relativistic and
even sub-relativistic regime. In this case the nice analytical properties
of the synchrotron emission mechanism must be replaced by the more accurate
\ac{mbs} emission. As we shall see in \Sref{sec:mbs}, a detailed treatment
of the spectral evolution of electrons emitting \ac{mbs} radiation requires
involved numerical calculations of integrals performed over sums of
infinite series (each of the series terms corresponding to a different
harmonic of the gyrofrequency). In this section we review some previous
efforts to compute the \ac{mbs} in astrophysical plasma. The numerical
details of such efforts are discussed more throughly in \Sref{sec:mbs}.

Analytic expressions for the transrelativistic regime were found by
\citet{Wild:1971hi} using accurate approximations of the Bessel
functions. Their approximations have been useful for later works building
more accurate expressions for the \ac{mbs} emission and absorption
\citep[e.g.][]{Robinson:1984me,Klein:1987mi,Wardzinski:2000mn,Fleishman:2010ku}. 
Later \citet{Petrosian:1981ij} approximated the power radiated
(\Eref{eq:eta-nu}) for a single electron replacing sums of harmonic
contributions by integrals over a continuous distribution of such
harmonics. Such approximation is valid in the regime where the harmonics
are so close to each other that they are indistinguishable; i.e., for the
cases where the frequency is much larger than the gyrofrequency (see
\Sref{sec:transrel-regime}). This approach produces a typical relative
numerical error between 20 and 30\%. Nevertheless, it has been used in
different works afterwards
\citep[e.g.][]{Ghisellini:1988nw,Wardzinski:2000mn,Fleishman:2010ku}.
Unfortunately, it does not deal with the harmonics in the \ac{mbs} emission
which appear at low frequencies; i.e, near the gyrofrequency, and it is
precisely that spectral range that is of interest for this thesis.

On the other hand, over the years numerical methods and techniques that
deal with the full cyclosynchrotron emission have been improving. An
extensive and concise numerical approach was first performed by
\citet{Brainerd:1987aj}. This method was implemented to calculate the
emissivity from a thermal, nonthermal and hybrid distributions in \acp{grb}
and applied to fit the photon flux obtained from observations, achieving
accurate results. \citet{Mahadevan:1996ek} and later in
\citet{Wolfe:2006ek} artificially \emph{broadened} the harmonics by a small
amount in order to facilitate the numerics.

\citet{Peer:2005wa} developed a code with split regimes in which for
frequencies 200 times the gyrofrequency and Lorentz factors lower than 10,
the full expression for \ac{mbs} (see \Sref{sec:radiated-power}) was
computed, and above that threshold the classical synchrotron expression
\citep[e.g.][]{Rybicki:1979,Jackson:1999} was employed. A similar
approached was made by \citet{Fleishman:2010ku} by placing a frequency
boundary below which the harmonic structure is recovered, and above which
the analytic expressions found by \citet{Petrosian:1981ij} and
\citet{Wild:1971hi} are used.

One of the main difficulties in the computation of the \ac{mbs} emission
stems from the interplay between integrals containing Dirac
$\delta$-functions with non-trivial arguments and the presence of sums over
series of harmonics. Solving this difficulty numerically may be done by
either performing analytically those integrals containing the Dirac
$\delta$-functions or employing the Dirac $\delta$-functions and using that
to compute the limits for the sums over harmonics. The latter option has
the advantage that it limits analytically the number of terms to be added
up in a sum, which may have an infinite number of contributing terms (a
priori), and for which there are no mathematical criteria to ensure
numerical convergence. This methodology has been used recently, and proved
to be an accurate approach for calculating the harmonics
\citep[e.g.][]{Marcowith:2003ji,Leung:2011aj,Pandya:2016zh,Rueda:2017hd}


\section{Motivation}
\label{sec:motivation}

In an attempt to understand blazar observations, many models and hypothesis
have been proposed over the years, giving us different ideas about the
physics involved in high energy processes. Still, there is not enough
observational evidence that can tell us the precise level of importance
that each physical processes has in the production of the observed high
energy emission (e.g., it is well known that magnetic fields may play an
important role in relativistic outflows, but we do not know the jet
magnetization nor do we know with certainty whether the magnetic fields
play any role at all role in the dissipation processes in the jet). There
are models that have been proposed to classify and unify \acp{agn} (and
blazars in particular). However, we do not know with certainty that these
models describe the true nature of \acp{agn}.

We live in an era when the existing and the upcoming observatories all
around the world (including those in space) observe the universe in many
spectral bands so that we expect that in the next years much more
information about all kinds of objects (blazars among them) will be
obtained. However, the impossibility of replicating in the laboratory the
necessary conditions to observe and measure the process that produces e.g.,
blazars flares has favoured the continuous development of sophisticated
numerical codes that perform simulations of these processes. These
simulation help us obtain a physical insight into the astrophysical
phenomena (either by comparison with the existing observational data or by
predicting the properties of future events). The state of the art codes for
blazars incorporate as much macro- and microphysics as the computational
capabilities allow. Typically, a trade-off between the two exists; i.e.,
one needs to decide how much effort to devote to the large scales (e.g.,
\ac{mhd} processes), and how much to the small scales (e.g., shock
acceleration). In our work we constantly try to improve our modeling on
both sides of the dynamic range of scales present in blazars.

The \ac{is} model for blazars has succeeded in modeling observational data
\citep[e.g.][]{Bottcher:2010gn}.  In the present thesis we attempt, based
on previous works
\citep{Bottcher:2010gn,Mimica:2004phd,Mimica:2012aa,Mahadevan:1996ek,Leung:2011aj},
to go further in exploring the \ac{is} scenario in order to find the
fingerprint in the \acp{sed} and light-curves of the shell magnetization
(macro-scales), and of the properties of the particles injected at the
shock front (micro-scales). We take into account the existence of sub- and
transrelativistic particles in the injection, and therefore a full
cyclotron, synchrotron and cyclo-synchrotron emission processes are
considered.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "phd"
%%% reftex-default-bibliography: ("./phd.bib")
%%% End:

