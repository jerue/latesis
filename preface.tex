\chapter{Preface}
\label{cha:preface}

This thesis aims to reveal the physics underlying the phenomenology
observed in blazars. The reasons why blazars find themselves among the most
appealing Astrophysical objects known and why they are fascinating objects
that have stimulated the creativity of several generations of astronomers
and astrophysicists will be addressed in \Cref{ch:intro}.

In \Cref{cha:theor-backgr} we will elaborate on the physical background on
which the \ac{mbs} and the blazars \ac{is} model resides. We will start
with the dynamics and electrodynamics of a charged particle immersed in a
homogeneous magnetic field. We will continue with the description of the
dynamics of colliding shells in the \ac{is} model, based on
\citet{Mimica:2012aa}. The last part of this chapter gives an overview of
the different types of distributions of particles, the model employed for
the injection of particles at a shock front and the evolution of particles
in a shocked region in two regimes: with and without including a radiative
cooling term active over a finite period of time.

In \Cref{cha:blazars-code} we will enclose a detailed description of the
numerical techniques and methods developed and used during my stay in the
\ac{camap} research group with Prof. Miguel \'{A}ngel Aloy and Dr. Petar
Mimica. The first part of that chapter is focused on the Internal Shocks
code developed by \citet{Mimica:2012aa}, which is the cornerstone of the
present work. In the second part we present a detailed description of the
code \chamba: a new computational tool which intends to reproduce the
emissivity out of single charged particles moving with arbitrary speed, and
also of distributions of particles of arbitrary profile in a magnetic
field. Our published works \citep{Rueda:2014mn,Rueda:2017hd} are results
out of a systematic and consistent use of these tools.

\Cref{cha:prmint} is based on research performed in the \ac{camap} research
group with Prof. Mi\-guel A. Aloy and Dr. Petar Mimica, as a continuation
of the previously published work \citep{Mimica:2012aa}. The work was
published in 2014: \bibentry{Rueda:2014mn}. The work consisted in expanding
the parameter space for the internal shocks model of blazars previously
scanned with the code developed by Dr. Petar Mimica and Prof. Miguel
A. Aloy. The aforementioned code was supplemented with the possibility of
computing the photons spectral index of the synthetic models (described in
\Sref{sec:photon-index}). The observational data was obtained from the
\ac{2lac} database%
\footnote{\url{https://heasarc.gsfc.nasa.gov/W3Browse/all/fermilac.html}}.
We performed all the simulations in this chapter in the supercomputer
\texttt{Tirant}.

\Cref{cha:hybdis} is based on research performed in the \ac{camap} research
group with Prof. Mi\-guel A. Aloy and Dr. Petar Mimica. The work was
published in 2017: \bibentry{Rueda:2017hd}. The results described
correspond to simulations made by using the internal shocks code
\citep{Mimica:2012aa} with a hybrid thermal-nonthermal particles
distribution injected (\Sref{sec:hyb-ther-nonther}), the finite-time
particles evolution scheme (\Sref{sec:power-law-source}) and the new
numerical tool \texttt{CHAMBA} (\Sref{sec:chamba}). All the simulations
where performed using the servers of the Department of Astronomy of
Astrophysics of the University of Valencia: \texttt{Fujiserver1} and
\texttt{ARC1}.

Astrophysics has meant to me like walking into the wild. I never knew what
I was going to find but every step has brought joy and
enlightenment. Knowing a bit of the phenomena which take place out there
has been like breathing fresh air in the woods, which enriches your lungs
and brings you peace, or like a heavy rain, which soaks you with the vital
liquid but it is too much that you have to run for shelter. The process of
proposing the development of new numerical tools like \chamba, has helped
me to get a feeling of what is out there. The process of writing it, on the
other hand, has been like climbing a mountain: you never know if you are
going to get there or if nature is going to send a storm and make you draw
back. We started to climb from high-energy-electrons base camp. We planned
the route there, keeping an eye on the weather at all moment for any storm
forecast. During the days in base camp we had to deal with fundamental
questions like whether it is worth it to go beyond the transrelativistic
heights, which so many explorers have gone up and down with great skill
over and over for decades, or stay at the foothills of \mbs~mountain. With
a \emph{blazarian} impulse we decided to leave the tranquility of base camp
and hit the crag. We came across unstable (numerical) gravel ravines, climb
and rappel steep cliffs and monumental crags. Ergs and ergs of vertical
walls. There were moments where no grip was on sight, praying for the rope
to hold. Fortunately for our expedition it did, and we made it to the
cyclotron peak.

\begin{flushright}
  JMRB
\end{flushright}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "chap"
%%% End:
