import numpy as np
import PypersPlots as pp
import extractor as extr
from logfuns import log1,log2,log3,log4,log5

#######  P function  ########
def ibpGTeps(a,s):
    return (1.0 - a**(1.0 - s)) / (s - 1.0)

def ibpLTeps(a,s):
    return np.log(a) - 0.5 * np.log(a)**2 * (s - 1.0)

def ibpEPS(a,s):
    return np.log(a)**3 * (s - 1.0)**2 / 6.0 

#######  Q function  ########
def ibqGTeps(a,s):
    return (1.0 - a**(1.0 - s) * (1.0 + (s - 1.0) * np.log(a))) / (s - 1.0)**2

def ibqLTeps(a,s):
    return 0.5 * np.log(a)**2 - (1.0 / 3.0) * np.log(a)**3 * (s - 1.0)

def ibqEPS(a,s):
    return 0.125 * np.log(a)**4 * (s - 1.0)**2

#######  Q2 function  ########
def ibq2GTeps(a,s):
    return (2.0 * a**s + a * ( (s - 1.0) * np.log(a) * ( np.log(a) - s * np.log(a) - 2.0 ) - 2.0 )) / (a**s * (s - 1.0)**3)

def ibq2LTeps(a,s):
    return np.log(a)**3 / 3.0 - 0.25 * (s - 1.0) * np.log(a)**4

def ibq2EPS(a,s):
    return 0.1 * np.log(a)**5 * (s - 1.0)**2


########  Starred functions  #########
def Pstar(x,s,eps):
    if type(x) is float:
        e = ibpEPS(x,s)
        if e > eps:
            return ibpGTeps(x,s)
        else:
            return log1(x,eps)-0.5*log2(x,eps)*(s-1.0)
    else:
        fstar = []
        for ex in x:
            e = ibpEPS(ex,s)
            if e > eps:
                fstar.append(ibpGTeps(ex,s))
            else:
                fstar.append(log1(ex,eps)-0.5*log2(ex,eps)*(s-1.0))
        return np.array(fstar)


def Qstar(x,s,eps):
    if type(x) is float:
        e = ibqEPS(x,s)
        if e > eps:
            return ibqGTeps(x,s)
        else:
            return 0.5*log2(x,eps)-log3(x,eps)*(s-1.0)/3.0
    else:
        fstar = []
        for ex in x:
            e = ibqEPS(ex,s)
            if e > eps:
                fstar.append(ibqGTeps(ex,s))
            else:
                fstar.append(0.5*log2(ex,eps)-log3(ex,eps)*(s-1.0)/3.0)
        return np.array(fstar)

def Q2star(x,s,eps):
    if type(x) is float:
        e = ibq2EPS(x,s)
        if e > eps:
            return ibq2GTeps(x,s)
        else:
            return log3(x,eps)/3.0-0.25*log4(x,eps)*(s-1.0)
    else:
        fstar = []
        for ex in x:
            e = ibq2EPS(ex,s)
            if e > eps:
                fstar.append(ibq2GTeps(ex,s))
            else:
                fstar.append(log3(ex,eps)/3.0-0.25*log4(ex,eps)*(s-1.0))
        return np.array(fstar)

############################################################################

ibfdir = "/Users/jesus/lab/cSPEV/tests/pwlInteg2/"

pp.latexify(fscale=1.0, ratio=1.2, txtwdth=369.0, lw=0.7)
fig, ax = pp.initPlot()

ls1 = [2.5,1.5]
ls2 = [1,1]
ls3 = [1,1,3,1]

x1 = np.logspace(-7.0,0.0,500)
x = x1 + 1.0

ax.set_yscale('log')
ax.set_xscale('log')

#ax.text(5e-4, 0.012, "$s = 1.001$", fontsize=14)
#ax.text(5e-4, 0.05, r"$s = 1.0001$", fontsize=14)

s = 1.001
p1, = ax.plot(x1, np.abs(1.0 - ibpGTeps(x,s)/Pstar(x,s,1e-5)), lw=1.0, c='k')
ax.plot(x1, np.abs(1.0 - ibqGTeps(x,s)/Qstar(x,s,1e-5)), lw=1.0, c='r')
ax.plot(x1, np.abs(1.0 - ibq2GTeps(x,s)/Q2star(x,s,1e-5)), lw=1.0, c='b')

s = 1.0001
p2, = ax.plot(x1, np.abs(1.0 - ibpGTeps(x,s)/Pstar(x,s,1e-5)), lw=1.0, c='k', ls='--')
ax.plot(x1, np.abs(1.0 - ibqGTeps(x,s)/Qstar(x,s,1e-5)), lw=1.0, c='r', ls='--')
ax.plot(x1, np.abs(1.0 - ibq2GTeps(x,s)/Q2star(x,s,1e-5)), lw=1.0, c='b', ls='--')

ax.legend([p1, p2],
          [r"$s - 1 = 10^{-3}$",
           r"$s - 1 = 10^{-4}$"],
          loc='best',
          frameon=False,
          fontsize=12,
          borderaxespad=0.8,
          labelspacing=0.5
)

pp.decor(ax,
         ylim=(5e-8,10.0),
         xlim=(1e-7, 1.0),
         xlabel=r"$x$",
         ylabel="Relative Error",
         labels_kw={'size' : 18},
         ticks_kw={'labelsize' : 12}
)
############################################################################



############################################################################
#           PRINTING OF THE PLOT
############################################################################

#fig.tight_layout()
pp.printer(fig, 'PQQ2funcs', savedir='/Users/jesus/lab/LaTesis/chambaFigures/', printEPS=True, printPDF=False, printPGF=False, tight=True)
