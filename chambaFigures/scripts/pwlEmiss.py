import matplotlib.pyplot as plt
import extractor as extr
import PypersPlots as pp

pp.latexify(fscale=1.0, txtwdth=369.0, lw=0.7)
fig, ax = pp.initPlot()

dirI3 = "/Users/jesus/lab/cSPEV/tests/I3tests/thesisFigs/"

ax.set_xscale('log')
ax.set_yscale('log')

lc1 = '#1f77b4'
lc2 = '#ff7f0e' #'#e39e54'
lc3 = '#2ca02c' #'#d64d4d'
lc4 = '#d62728' #'#4d7358'
lc5 = '#9467bd'
lc6 = '#8c564b'

nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+2.1.dat',cols=(0,1))
p1, = ax.plot(nu,jnu, c=lc1, lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+2.1.dat',cols=(0,2))
ax.plot(nu,jnu, c=lc1, ls='--', lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+2.5.dat',cols=(0,1))
p2, = ax.plot(nu,jnu,c=lc2, lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+2.5.dat',cols=(0,2))
ax.plot(nu,jnu,c=lc2,ls='--', lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+2.9.dat',cols=(0,1))
p3, = ax.plot(nu,jnu,c=lc3, lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+2.9.dat',cols=(0,2))
ax.plot(nu,jnu,c=lc3,ls='--', lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+3.3.dat',cols=(0,1))
p4, = ax.plot(nu,jnu,c=lc4, lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+3.3.dat',cols=(0,2))
ax.plot(nu,jnu,c=lc4,ls='--', lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+3.7.dat',cols=(0,1))
p5, = ax.plot(nu,jnu,c=lc5, lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+3.7.dat',cols=(0,2))
ax.plot(nu,jnu,c=lc5,ls='--', lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+4.1.dat',cols=(0,1))
p6, = ax.plot(nu,jnu,c=lc6, lw=1.0)
nu,jnu = extr.dataExtract2col(dirI3 + 'jnu.q=+4.1.dat',cols=(0,2))
ax.plot(nu,jnu,c=lc6,ls='--', lw=1.0)

pp.decor(ax,
         ylim=(1e-27, 2e-23),
         xlim=(1e5, 1e9),
         xlabel=r"$\nu'\: [\mathrm{Hz}]$",
         ylabel=r"$j_{\nu'}'\: [\mathrm{erg\, cm}^{-2}\, \mathrm{s}^{-1}\, \mathrm{Hz}^{-1}]$",
         labels_kw={'size' : 'x-large'},
         ticks_kw={'labelsize' : 13})

ax.legend([p1, p2, p3, p4, p5, p6],
          [r"$q = 2.1$", r"$q = 2.5$", r"$q = 2.9$", r"$q = 3.3$", r"$q = 3.7$", r"$q = 4.1$"],
          loc='best',
          frameon=False,
          fontsize=11,
          borderaxespad=0.8
)

#fig.tight_layout()
pp.printer(fig, 'pwl-emiss', savedir='/Users/jesus/lab/LaTesis/chambaFigures/', printEPS=True, printPDF=False, printPGF=False, tight=True)
