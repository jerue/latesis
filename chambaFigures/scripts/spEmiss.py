import PypersPlots as pp
import extractor as ext
import numpy as np
import os

dir1 = "/Users/jesus/lab/cSPEV/tests/CycSyn_tests/kernels/Gauss/"
dir2 = '/Users/jesus/lab/cSPEV/tests/CycSyn_tests/Results/spEmiss/'

pp.latexify(txtwdth=369.0, lw=0.8)

fig, ax = pp.initPlot()

ax.set_xscale('log')
ax.set_yscale('log')


c, n = np.loadtxt(dir1 + 'beta0p2_Gauss_kappa5e-3.dat', unpack=True)
ax.plot(c, n, c='k', ls='--')
c, n = np.loadtxt(dir2 + 'rawCHAMBA:gamma0001-narrow.dat', unpack=True)
plt1, = ax.plot(c, n, c='k')

c, n = np.loadtxt(dir1 + 'beta0p4_Gauss_kappa5e-3.dat', unpack=True)
ax.plot(c, n, c='r', ls='--')
c, n = np.loadtxt(diremiss + 'rawCHAMBA:gamma0002-narrow.dat', unpack=True)
plt2, = ax.plot(c, n, c='r')

c, n = np.loadtxt(dir1 + 'beta0p8_Gauss_kappa5e-3.dat', unpack=True)
ax.plot(c, n, c='b', ls='--')
c, n = np.loadtxt(diremiss + 'rawCHAMBA:gamma0003-narrow.dat', unpack=True)
plt3, = ax.plot(c, n, c='b')

ax.legend([plt1, plt2, plt3], [r"$\beta = 0.2$", r"$\beta = 0.4$", r"$\beta = 0.8$"],loc='best')#, loc=1, fontsize=10)
pp.decor(ax, ylim=(1e-6,1.), ylabel=r"$\tilde{I}_1(\mathcal{X}, \beta)$", xlabel=r"$\mathcal{X}$",xlim=(0.1,1e2))

pp.printer(fig, 'spEmiss', savedir='/Users/jesus/lab/LaTesis/chambaFigures/', printEPS=True, printPDF=False, printPGF=False)#,onscreen=True)#
