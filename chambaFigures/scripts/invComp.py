import ibfuncs as ib
import numpy as np
import PypersPlots as pp
from matplotlib import ticker

eV = 1.6021772e-12
Planckh = 6.6260755e-27/eV

ibfdir = "/Users/jesus/lab/cSPEV/tests/pwlInteg2/"

pp.latexify(fscale=1.0, txtwdth=369.0, lw=0.7, ratio=1.4)

fig, ax1 = pp.initPlot()
ax2 = ax1.twinx()
ax3 = ax1.twiny()

ls1 = [0.5,1]
ls4 = [3,1.5]
ls2 = [0.5,1,3.5,1]
ls3 = [0.5,1,0.5,1,3.5,1]

ax1.set_yscale('log')
ax2.set_yscale('log')
ax1.set_xscale('log')
ax3.set_xscale('log')

############################################################################
nu, jISOdel = np.loadtxt(ibfdir + "jISOdel-1p1.dat", unpack=True)
nu, jISOpwl = np.loadtxt(ibfdir + "jISOpwl-1p1.dat", unpack=True)
ax1.plot(nu, jISOdel, 'k', lw=1.0)
ax2.plot(nu, jISOpwl, 'r', lw=1.0)

nu, jISOdel = np.loadtxt(ibfdir + "jISOdel-1p5.dat", unpack=True)
nu, jISOpwl = np.loadtxt(ibfdir + "jISOpwl-1p5.dat", unpack=True)
p1, = ax1.plot(nu, jISOdel, 'k', lw=1.0)
p2, = ax2.plot(nu, jISOpwl, 'r', lw=1.0)
p1.set_dashes(ls1)
p2.set_dashes(ls1)

nu, jISOdel = np.loadtxt(ibfdir + "jISOdel-2p25.dat", unpack=True)
nu, jISOpwl = np.loadtxt(ibfdir + "jISOpwl-2p25.dat", unpack=True)
p1, = ax1.plot(nu, jISOdel, 'k', lw=1.0)
p2, = ax2.plot(nu, jISOpwl, 'r', lw=1.0)
p1.set_dashes(ls4)
p2.set_dashes(ls4)

nu, jISOdel = np.loadtxt(ibfdir + "jISOdel-3p0.dat", unpack=True)
nu, jISOpwl = np.loadtxt(ibfdir + "jISOpwl-3p0.dat", unpack=True)
p1, = ax1.plot(nu, jISOdel, 'k', lw=1.0)
p2, = ax2.plot(nu, jISOpwl, 'r', lw=1.0)
p1.set_dashes(ls2)
p2.set_dashes(ls2)

nu, jISOdel = np.loadtxt(ibfdir + "jISOdel-4p0.dat", unpack=True)
nu, jISOpwl = np.loadtxt(ibfdir + "jISOpwl-4p0.dat", unpack=True)
p1, = ax1.plot(nu, jISOdel, 'k', lw=1.0)
p2, = ax2.plot(nu, jISOpwl, 'r', lw=1.0)
p1.set_dashes(ls3)
p2.set_dashes(ls3)

############################################################################
# ------  nu_ext  ------
ax1.axvline(x=1e14, c='m')
ax1.text(7e13, 3.0, r"$\nu_{\mathrm{ext}}'$", fontsize=11, rotation=270, horizontalalignment='right', color='m')
# ------  4 nu_ext  ------
ax1.axvline(x=4*1e14, c='g')
ax1.text(4e14, 2e-3, r"$4\nu_{\mathrm{ext}}'$", fontsize=11, rotation=270, color='g')
# ------  nu_2  ------
ax1.axvline(x=1e16, c='m')
ax1.text(6e15, 3e0, r"$\nu_{2}'$", fontsize=11, horizontalalignment='right', rotation=270, color='m')
# ------  4 nu_2  ------
ax1.axvline(x=4*1e16, c='g')
ax1.text(2.6e16, 4e-5, r"$4 \nu_{2}'$", fontsize=11, horizontalalignment='right', rotation=270, color='g')
# ------  4 gamma_min^2 nu_ext  ------
ax1.axvline(x=4.*1e14*15.**2, c='b')
ax1.text(1e17, 1.5e-2, r"$4 \gamma_{\min}^2 \nu_{\mathrm{ext}}'$", fontsize=11, rotation=270, color='b')
# ------  4 gamma_min^2 nu_2  ------
ax1.axvline(x=4.*1e16*15.**2, c='b')
ax1.text(8e17, 1.5e-4, r"$4 \gamma_{\min}^2 \nu_{2}'$", fontsize=11, rotation=270, color='b')
# ------  4 gamma_max^2 nu_ext  ------
ax1.axvline(x=4.*1e14*3e6**2, c='b')
ax1.text(3e27, 3e-4, r"$4 \gamma_{\max}^2 \nu_{\mathrm{ext}}'$", fontsize=11, rotation=270, color='b')
# ------  4 gamma_max^2 nu_2  ------
ax1.axvline(x=4.*1e16*3e6**2, c='b')
ax1.text(3e28, 2.0, r"$4 \gamma_{\max}^2 \nu_{2}'$", fontsize=11, rotation=270, color='b')
# ------  4 (m_e c^2/h nu_ext)^2 nu_ext  ------
ax1.axvline(x=4.*1e14*(1.2340804e6)**2, c='dimgray')
ax1.text(3e26, 1.3e-3, r"$4 \left(\frac{m_{\mathrm{e}} c^2}{h \nu_{\mathrm{ext}}'}\right)^2 \nu_{\mathrm{ext}}'$", fontsize=11.8, horizontalalignment='right', rotation=270, color='dimgray')
############################################################################

xticks = []
for k in range(14,31,2):
    xticks.append(10**k)
ax1.xaxis.set_ticks(xticks)

pp.decor(ax1,
         ylim=(1e-5, 1e1),
         xlim=(1e13, 1e30),
         xlabel=r"$\nu'$ [Hz]",
         ylabel=r"$j_{\nu'}' \nu_{\mathrm{ext}}' \big/ c \sigma_{T}\, u_{\mathrm{ext}}' n(\gamma_{\min})$"
)

#### Secondary y-axis
pp.decor(ax2,ylim=(2e-6, 1e1))
ax2.set_ylabel(r"$j_{\nu'}' \big/ \sigma_{T} I_{\nu_{\mathrm{ext}}'}' n(\gamma_{\min})$", color='r')

##### Secondary x-axis
ax1.xaxis.tick_bottom()
ax3.xaxis.tick_top()

formatter = ax1.get_xaxis().get_major_formatter()
ax3.set_xlim(ax1.get_xlim())
x2ticks = []
x2labels = []
for k in range(-2,18,2):
    x2ticks.append(10**k/Planckh)
    x2labels.append(r'$10^{'+str(k)+"}$")
#ax3.xaxis.set_ticks(x2ticks)
ax3.xaxis.set_ticklabels(x2labels)
ax3.xaxis.set_major_locator(ticker.FixedLocator(x2ticks))
ax3.set_xlabel(r'Energy [eV]')

#fig.tight_layout()
pp.printer(fig, 'IC-emiss', savedir='/Users/jesus/lab/LaTesis/chambaFigures/', printEPS=True, printPDF=False, printPNG=True, printPGF=False,tight=True)
