import matplotlib as mpl
import PypersPlots as pp
import numpy as np
import extractor as ex

#x = np.logspace(-5.0, np.log10(0.2), num=100)
ibfdir = "/Users/jesus/lab/cSPEV/tests/pwlInteg2/"

pp.latexify(ratio=4.0/3.0, txtwdth=369.88582, lw=0.7)
fig, [ax1, ax2] = pp.initPlot(nrows=2, shareX=True, gskw={'height_ratios': [1.5, 1]})

ax1.set_yscale('log')
ax1.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xscale('log')

x, lf, ilf, rel = np.loadtxt(ibfdir + "log1.dat", unpack=True)
ax1.plot(x, ilf, c='k')
l1, = ax1.plot(x, lf, c='k')
ax2.plot(x, rel, c='k')
x, lf, ilf, rel = np.loadtxt(ibfdir + "log2.dat", unpack=True)
ax1.plot(x, ilf, c='r')
l2, = ax1.plot(x, lf, c='r')
ax2.plot(x, rel, c='r')
x, lf, ilf, rel = np.loadtxt(ibfdir + "log3.dat", unpack=True)
ax1.plot(x, lf, c='b')
l3, = ax1.plot(x, lf, c='b')
ax2.plot(x, rel, c='b')
x, lf, ilf, rel = np.loadtxt(ibfdir + "log4.dat", unpack=True)
ax1.plot(x, lf, c='g')
l3, = ax1.plot(x, lf, c='g')
ax2.plot(x, rel, c='g')
x, lf, ilf, rel = np.loadtxt(ibfdir + "log5.dat", unpack=True)
ax1.plot(x, lf, c='m')
l5, = ax1.plot(x, lf, c='m')
ax2.plot(x, rel, c='m')

l1.set_dashes([3,1])
l2.set_dashes([3,1])
l3.set_dashes([3,1])
l4.set_dashes([3,1])
l5.set_dashes([3,1])

pp.decor(ax1,
         #ylim=(1e-6,1e-2),
         ylabel="Logarithms")
pp.decor(ax2,
         #ylim=(1e-4,0.09),
         #xlim=(1e-5,0.2),
         ylabel="Relative error", xlabel="$x - 1$")

fig.tight_layout()
#pp.printer(fig, 'logFunctions', savedir='/Users/jesus/lab/LaTesis/chambaFigures/', printEPS=True, printPDF=False, printPGF=False)
