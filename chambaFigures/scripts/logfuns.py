import matplotlib as mpl
import PypersPlots as pp
import numpy as np

def log1(x, eps):
    def epsi(x):
        return 0.5 * (x - 1.0)**2
    if type(x) == float:
        if epsi(x) > eps:
            return np.log(x)
        else:
            return x - 1.0
    else:
        val = np.piecewise(x,
                           [epsi(x) > eps,
                            epsi(x) <= eps],
                           [lambda x: np.log(x),
                            lambda x: x - 1.0])
        return val

def log2(x, eps):
    if type(x) == float:
        if np.abs((x - 1.0)**3) > eps:
            return np.log(x)**2
        else:
            return (x - 1.0)**2
    else:
        val = np.piecewise(x,
                           [np.abs((x - 1.0)**3) > eps,
                            np.abs((x - 1.0)**3) <= eps],
                           [lambda x: np.log(x)**2,
                            lambda x: (x - 1.0)**2])
        return val
        
def log3(x, eps):
    if type(x) == float:
        if np.abs(1.5 * (x - 1.0)**4) > eps:
            return np.log(x)**3
        else:
            return (x - 1.0)**3
    else:
        val = np.piecewise(x,
                           [np.abs(1.5 * (x - 1.0)**4) > eps,
                            np.abs(1.5 * (x - 1.0)**4) <= eps],
                           [lambda x: np.log(x)**3,
                            lambda x: (x - 1.0)**3])
        return val

def log4(x, eps):
    if type(x) == float:
        if np.abs(2.0 * (x - 1.0)**5) > eps:
            return np.log(x)**4
        else:
            return (x - 1.0)**4
    else:
        val = np.piecewise(x,
                           [np.abs(2.0 * (x - 1.0)**5) > eps,
                            np.abs(2.0 * (x - 1.0)**5) <= eps],
                           [lambda x: np.log(x)**4,
                            lambda x: (x - 1.0)**4])
        return val

def log5(x, eps):
    def epsi(x):
        return np.abs(2.5 * (x - 1.0)**6)
    if type(x) == float:
        if epsi(x) > eps:
            return np.log(x)**5
        else:
            return (x - 1.0)**5
    else:
        val = np.piecewise(x,
                           [epsi(x) > eps,
                            epsi(x) <= eps],
                           [lambda x: np.log(x)**5,
                            lambda x: (x - 1.0)**5])
        return val

min1 = 0.1
x = np.linspace(1.0 - min1, 1.0 + min1, num=400)
x1 = 1.0 - x
eps = 1e-9

pp.latexify(ratio=1.2, txtwdth=369.88582, lw=0.7)
fig, [ax1, ax2] = pp.initPlot(nrows=2, shareX=True, gskw={'height_ratios': [1.5, 1.0]})

ax1.set_yscale('symlog', linthreshy=1e-8)
#ax1.set_xscale('log')
#ax2.set_xscale('symlog', linthreshx=1e-4)
ax2.set_yscale('log')

ax1.plot(x1, np.log(x), c='k')
ax1.plot(x1, np.log(x)**2, c='r')
ax1.plot(x1, np.log(x)**3, c='b')
ax1.plot(x1, np.log(x)**4, c='g')
ax1.plot(x1, np.log(x)**5, c='m')

l1, = ax1.plot(x1, log1(x, eps), c='k')
l2, = ax1.plot(x1, log2(x, eps), c='r')
l3, = ax1.plot(x1, log3(x, eps), c='b')
l4, = ax1.plot(x1, log4(x, eps), c='g')
l5, = ax1.plot(x1, log5(x, eps), c='m')
l1.set_dashes([3,1])
l2.set_dashes([3,1])
l3.set_dashes([3,1])
l4.set_dashes([3,1])
l5.set_dashes([3,1])

ax2.plot(x1, np.abs(1.0 - (log1(x, eps) / np.log(x))), c='k')
ax2.plot(x1, np.abs(1.0 - (log2(x, eps) / np.log(x)**2)), c='r')
ax2.plot(x1, np.abs(1.0 - (log3(x, eps) / np.log(x)**3)), c='b')
ax2.plot(x1, np.abs(1.0 - (log4(x, eps) / np.log(x)**4)), c='g')
ax2.plot(x1, np.abs(1.0 - (log5(x, eps) / np.log(x)**5)), c='m')

# ax2.plot(x1, np.abs(1.0 - (log1(x+1., eps) / np.log(x+1.))), c='k')
# ax2.plot(x, np.abs(1.0 - (log2(x+1., eps) / np.log(x+1.)**2)), c='r')
# ax2.plot(x1, np.abs(1.0 - (log3(x+1., eps) / np.log(x+1.)**3)), c='b')
# ax2.plot(x1, np.abs(1.0 - (log4(x+1., eps) / np.log(x+1.)**4)), c='g')
# ax2.plot(x1, np.abs(1.0 - (log5(x+1., eps) / np.log(x+1.)**5)), c='m')

pp.decor(ax1, ylim=(-1e-7,1e-4),xlim=(-0.1,0.1), ylabel="Logarithms")
pp.decor(ax2, ylim=(6e-5,0.6),
         #xlim=(-0.1,0.1),
         ylabel="Relative error", xlabel="$x-1$")
fig.tight_layout()

pp.printer(fig, 'logFunctions',
           #savedir='/Users/jesus/lab/LaTesis/chambaFigures/',
           savedir='/Users/jesus/Desktop/',
           printEPS=True, printPDF=False, printPGF=False)
