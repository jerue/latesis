import PypersPlots as pp
import extractor as ext
import numpy as np
import os

############################################################################
# KERNELS COMPARISON
############################################################################

def kernPlot(d,ax,num):
    lsdir = os.listdir(d)
    for item in lsdir:
        if item.startswith("beta0p2"):
            x,y = ext.dataExtract2col(d + item)
            if item.endswith("Gauss.dat"):
                k1, = ax.plot(x,y,'k',ls='-')
            if item.endswith("MNY96.dat"):
                k2, = ax.plot(x,y,'k',ls='--')
        if item.startswith("beta0p4") and item.endswith(".dat"):
            x,y = ext.dataExtract2col(d + item)
            if item.endswith("Gauss.dat"):
                k4, = ax.plot(x,y,'r',ls='-')
            if item.endswith("MNY96.dat"):
                ax.plot(x,y,'r',ls='--')
        if item.startswith("beta0p8") and item.endswith(".dat"):
            x,y = ext.dataExtract2col(d + item)
            if item.endswith("Gauss.dat"):
                k5, = ax.plot(x,y,'b',ls='-')
            if item.endswith("MNY96.dat"):
                ax.plot(x,y,'b',ls='--')
    if num == 1:
        return k1,k2
    elif num == 2:
        return k1,k4,k5

############################################################################
# KAPPA COMPARISON
############################################################################

def kappaPlot(d,ax,num):
    lsdir = os.listdir(d)
    for item in lsdir:
        if item.startswith("beta0p2"):
            x,y = ext.dataExtract2col(d + item)
            if item.endswith("a5e-2.dat"):
                k1, = ax.plot(x,y,'k',ls='-')
            if item.endswith("a5e-3.dat"):
                k2, = ax.plot(x,y,'k')
                k2.set_dashes([1,1])
            if item.endswith("a2p5e-3.dat"):
                k3, = ax.plot(x,y,'k')
                k3.set_dashes([3,1])
        if item.startswith("beta0p4") and item.endswith(".dat"):
            x,y = ext.dataExtract2col(d + item)
            if item.endswith("a5e-2.dat"):
                k4, = ax.plot(x,y,'r',ls='-')
            if item.endswith("a5e-3.dat"):
                k4d, = ax.plot(x,y,'r')
                k4d.set_dashes([1,1])
            if item.endswith("a2p5e-3.dat"):
                k4s, = ax.plot(x,y,'r')
                k4s.set_dashes([3,1])
        if item.startswith("beta0p8") and item.endswith(".dat"):
            x,y = ext.dataExtract2col(d + item)
            if item.endswith("a5e-2.dat"):
                k5, = ax.plot(x,y,'b')
            if item.endswith("a5e-3.dat"):
                k5d, = ax.plot(x,y,'b',ls='--')
                k5d.set_dashes([1,1])
            if item.endswith("a2p5e-3.dat"):
                k5s, = ax.plot(x,y,'b')
                k5s.set_dashes([3,1])
    if num == 1:
        return k1,k2,k3
    elif num == 2:
        return k1,k4,k5


############################################################################
# EPSILON COMPARISON
############################################################################
def epsPlot(d,ax,num):
    lsdir = os.listdir(d)
    for item in lsdir:
        if item.startswith("beta0p2"):
            x,y = ext.dataExtract2col(d + item)
            if item.endswith("eps1e-2.dat"):
                k1, = ax.plot(x,y,'k',ls='-')
            if item.endswith("eps1e-4.dat"):
                k2, = ax.plot(x,y,'k')
                k2.set_dashes([1,1])
            if item.endswith("eps1e-8.dat"):
                k3, = ax.plot(x,y,'k')
                k3.set_dashes([3,1])
        if item.startswith("beta0p4"):
            x,y = ext.dataExtract2col(d + item)
            if item.endswith("eps1e-2.dat"):
                k4, = ax.plot(x,y,'r',ls='-')
            if item.endswith("eps1e-4.dat"):
                k4d, = ax.plot(x,y,'r')
                k4d.set_dashes([1,1])
            if item.endswith("eps1e-8.dat"):
                k4s, =ax.plot(x,y,'r')
                k4s.set_dashes([3,1])
        if item.startswith("beta0p8"):
            x,y = ext.dataExtract2col(d + item)
            if item.endswith("eps1e-2.dat"):
                k5, = ax.plot(x,y,'b',ls='-')
            if item.endswith("eps1e-4.dat"):
                k5d, = ax.plot(x,y,'b')
                k5d.set_dashes([1,1])
            if item.endswith("eps1e-8.dat"):
                k5s, = ax.plot(x,y,'b')
                k5s.set_dashes([3,1])
    if num == 1:
        return k1,k2,k3
    elif num == 2:
        return k1,k4,k5

############################################################################
# PLOTTING SESSION
############################################################################

dir1 = "/Users/jesus/lab/cSPEV/tests/CycSyn_tests/kernels/Gauss/"
dir2 = "/Users/jesus/lab/cSPEV/tests/CycSyn_tests/kernels/MNY96/"
dir3 = "/Users/jesus/lab/cSPEV/tests/CycSyn_tests/kernels/"

pp.latexify(txtwdth=369.0, lw=0.8)

##### EMISS_EPS #####
fig, [ax1, ax2] = pp.initPlot(ncols=2, shareY=True)

ax1.set_xscale('log')
ax1.set_yscale('log')
k1 = epsPlot(dir1,ax1, 1)
ax1.legend([k1[0], k1[1], k1[2]], [r"$\varepsilon = 10^{-2}$", r"$\varepsilon = 10^{-4}$", r"$\varepsilon = 10^{-8}$"], loc=1, fontsize=8)
pp.decor(ax1, ylim=(1e-6,1.), ylabel=r"$P_{\nu'}'(\beta) c / 2 \pi e^{2} \nu_{\mathrm{g}}$", xlabel=r"$\mathcal{X}$", xlim=(0.1,2e2))

ax2.set_xscale('log')
ax2.set_yscale('log')
k2 = epsPlot(dir2,ax2, 2)
xminor_ticks = []
for k in range(-2,2):
    for i in range(2,10):
       xminor_ticks.append(i*10**k)
ax2.xaxis.set_ticks(np.logspace(-2., 2., num=5))
ax2.xaxis.set_ticks(xminor_ticks, minor=True)
ax2.legend([k2[0], k2[1], k2[2]], [r"$\beta = 0.2$", r"$\beta = 0.4$", r"$\beta = 0.8$"], loc=1, fontsize=8)
pp.decor(ax2, ylim=(1e-6,1.), xlim=(7e-2,1e2), xlabel=r"$\mathcal{X}$")

pp.printer(fig, 'emiss_eps', savedir='/Users/jesus/lab/LaTesis/chambaFigures/kernelSetup/', printEPS=True, printPDF=False, printPGF=False)#,onscreen=True)#

######### EMISS_KAPPA #########
fig, [ax1, ax2] = pp.initPlot(ncols=2,shareY=True)

ax1.set_xscale('log')
ax1.set_yscale('log')
k1 = kappaPlot(dir1,ax1, 1)
ax1.legend([k1[0], k1[1], k1[2]], [r"$\kappa = 0.05$", r"$\kappa = 0.005$", r"$\kappa = 0.0025$"], loc=1, fontsize=8)
pp.decor(ax1, ylim=(1e-6,1.), ylabel=r"$P_{\nu'}'(\beta) c / 2 \pi e^{2} \nu_{\mathrm{g}}$", xlabel=r"$\mathcal{X}$",xlim=(0.1,2e2))

ax2.set_xscale('log')
ax2.set_yscale('log')
k2 = kappaPlot(dir2,ax2, 2)
ax2.xaxis.set_ticks(np.logspace(-2., 2., num=5))
ax2.xaxis.set_ticks(xminor_ticks, minor=True)
ax2.legend([k2[0], k2[1], k2[2]], [r"$\beta = 0.2$", r"$\beta = 0.4$", r"$\beta = 0.8$"], loc=1, fontsize=8)
pp.decor(ax2, ylim=(1e-6,1.), xlim=(7e-2,1e2), xlabel=r"$\mathcal{X}$")

pp.printer(fig, 'emiss_kappa', savedir='/Users/jesus/lab/LaTesis/chambaFigures/kernelSetup/', printEPS=True, printPDF=False, printPGF=False)#,onscreen=True)#

##### EMISS_KERNELS #####
fig, ax1 = pp.initPlot()
ax1.set_xscale('log')
ax1.set_yscale('log')
k1, k2 = kernPlot(dir3,ax1, 1)
ax1.legend([k1, k2], ["Gaussian", "MNY96"], loc=(0.7, 0.8), fontsize=10)
pp.decor(ax1, ylim=(1e-6,1.), ylabel=r"$P_{\nu'}'(\beta) c / 2 \pi e^{2} \nu_{\mathrm{g}}$", xlabel=r"$\mathcal{X}$",xlim=(0.1,1e2))
pp.printer(fig, 'emiss_kernels', savedir='/Users/jesus/lab/LaTesis/chambaFigures/kernelSetup/', printEPS=True, printPDF=False, printPGF=False)#,onscreen=True)#
