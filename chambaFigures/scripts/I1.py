#!/usr/bin/env python -tt
from __future__ import (absolute_import, division, print_function)

import PypersPlots as pp
import extractor as extr
import colorcet as cc
import numpy as np
import time
import os
from matplotlib.ticker import AutoMinorLocator,LogLocator
import cubehelix as ch


dirin = "/Users/jesus/lab/cSPEV/tests/I1Error/"
tabName = dirin + "I1Error500x500.h5"

chi, gamma = extr.hdf5Extract1D(tabName, ['chi1D', 'gamma1D'])
I1, ER, RMA, RMAER,I1tab = extr.hdf5Extract2D(tabName, ['I1exact', 'errors', 'RMA', 'RMAerrors', 'I1table'])

cbmin = 1e-6
cbmax = 1e0
v1label = r"$\gamma$"
v2label = r"$\mathcal{X}$"
I1label = r"$\tilde{I}_{1}$"
ERlabel = r"Relative Error"

thecmap = [
    ch.cmap(reverse=True, start=2.5, rot=4.5, gamma=0.8, sat=1.8, minLight=0.1, maxLight=0.8),
    'viridis',
    'gist_stern',
    'inferno',
    'magma',
    'plasma',
    'gnuplot'
]

pp.latexify(txtwdth=369.0, ratio=4.0/3.0, lw=0.7)#, edgecol='w')
fig, ax = pp.initPlot()

ax.set_xscale('log')
ax.set_yscale('log')

I1CM = pp.theGradient(ax, gamma, chi, I1tab.T, (cbmin, cbmax),
                      LNorm=True,
                      cmap=thecmap[1]# + '_r'
)
# ax.axvline(x=20, color='k', alpha=0.3, ls=':')
# ax.axhline(y=100, color='k', alpha=0.3, ls=':')

CB = pp.setColorBar(I1CM, fig, ax,
                    cblabel = I1label,
                    blw = 0.7,
                    borders = False,
                    label_kw={'size' : 'xx-large'},
                    ticks_kw={'labelsize' : 14}
)

# yminor_ticks = []
# for k in range(-3,9):
#     for i in range(2,9,2):
#        yminor_ticks.append(i*10**k)
# ax.yaxis.set_ticks(np.logspace(-3., 8., num=12))
# ax.yaxis.set_ticks(yminor_ticks, minor=True)

pp.decor(ax,
         xlim=(1e0, 1e3),
         xlabel=r"$\gamma$",
         ylim=(1e-3, 1e8),
         ylabel=r"$\mathcal{X}$",
         labels_kw={'size' : 'xx-large'},
         ticks_kw={'labelsize' : 14}
)

d1 = '/Users/jesus/lab/LaTesis/chambaFigures/'#'/Users/jesus/Desktop/'#

fig.tight_layout()
#pp.printer(fig, 'spTable', savedir=d1, printEPS=True, printPDF=False, printPNG=False, printPGF=False, tight=True)#, savedir=d1, pgfdir='Figures/I1Table/')#, onscreen=True)#
