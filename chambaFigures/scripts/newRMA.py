import Synchrotron as sync
import numpy as np
import PypersPlots as pp
from logfuns import log1,log2,log3,log4,log5

def RMAfitA(x):
    return np.exp(
        - 0.787163
        - 0.705093 * log1(x, 1e-9)
        - 0.355319 * log2(x, 1e-9)
        - 0.0650331 * log3(x, 1e-9)
        - 0.00609012 * log4(x, 1e-9)
        - 0.000227646 * log5(x, 1e-9))

def RMAfitB(x):
    return np.exp(
        - 0.823646
        - 0.831669 * log1(x, 1e-9)
        - 0.52563 * log2(x, 1e-9)
        - 0.220393 * log3(x, 1e-9)
        + 0.0166918 * log4(x, 1e-9)
        - 0.0286507 * log5(x, 1e-9))
    
def RMAfit(x):
    def localRMA(x):
        if x < 3.21809e-4:
            return sync.asymRsync_low(x)
        elif x >= 3.21809e-4 and x <= 0.65053:
            return A(x)
        elif x > 0.65053 and x <= 15.5799:
            return B(x)
        else:
            return sync.asymRsync_high(x)
        
    if type(x) is float:
        return localRMA(x)
    else:
        rma = []
        for ex in x:
            rma.append(localRMA(ex))
        return np.array(rma)

def RMA(c, g):
    import numpy as np

    if c * g < 0.8:
        return 0.0
    else:
        x = 2.0 * chi / (3.0 * np.power(g, 2))
        return x * sync.SL07(x)

gb = 1.0 / np.sqrt(1.0 - np.power(0.1, 2))
c = np.logspace(-3.0, 8.0, num=200)
g = np.logspace(np.log10(gb), 4.0, num=200)
x = np.logspace(-4.0, 2.0, num=200)

pp.latexify(ratio=1.2, landscape=False, txtwdth=369.0, lw=0.7)
fig, [ax1, ax2] = pp.initPlot(nrows=2, shareX=True, gskw={'height_ratios': [1.5, 1]})

ax1.set_yscale('log')
ax1.set_xscale('log')

lc1 = '#1f77b4'
lc2 = '#ff7f0e' #'#e39e54'
lc3 = '#2ca02c' #'#d64d4d'
lc4 = '#d62728' #'#4d7358'

ax1.plot(x, sync.Rsync(x), lw=3.0, c='k')
ax1.plot(x, sync.SL07(x), lw=1.5, c=lc4)
ax1.plot(x, sync.asymRsync_low(x), c=lc3, lw=1.0)
ax1.plot(x, sync.asymRsync_high(x), ls='--', c=lc3, lw=1.0)
ax1.plot(x, sync.FDBfitA(x), c=lc2, lw=1.0)
ax1.plot(x, sync.FDBfitB(x), c=lc2, lw=1.0, ls='--')
ax1.plot(x, RMAfitA(x), c=lc1, lw=1.0)
ax1.plot(x, RMAfitB(x), c=lc1, lw=1.0, ls='--')

##### RELATIVE ERROR #####
ax2.set_yscale('log')

ax2.plot(x, np.abs(1.0 - sync.SL07(x)/sync.Rsync(x)), lw=1.5, c=lc4)
ax2.plot(x, np.abs(1.0 - sync.asymRsync_low(x)/sync.Rsync(x)), lw=1.0, c=lc3)
ax2.plot(x, np.abs(1.0 - sync.asymRsync_high(x)/sync.Rsync(x)), lw=1.0, c=lc3, ls='--')
ax2.plot(x, np.abs(1.0 - sync.FDBfitA(x)/sync.Rsync(x)), lw=1.0, c=lc2)
ax2.plot(x, np.abs(1.0 - sync.FDBfitB(x)/sync.Rsync(x)), lw=1.0, c=lc2, ls='--')
ax2.plot(x, np.abs(1.0 - RMAfitA(x)/sync.Rsync(x)), lw=1.0, c=lc1)
ax2.plot(x, np.abs(1.0 - RMAfitB(x)/sync.Rsync(x)), lw=1.0, c=lc1, ls='--')

##### BOTH PLOTS #####
ax1.axvline(x=3.218e-4, c='0.4', ls=':')
ax2.axvline(x=3.218e-4, c='0.4', ls=':')
ax1.text(3.6e-4, 0.02, r"$0.00032$", fontsize=13)
ax1.axvline(x=0.68, c='0.4', ls=':')
ax2.axvline(x=0.68, c='0.4', ls=':')
ax1.text(0.8, 5e-3, r"$0.65$", fontsize=13)
ax1.axvline(x=15, c='0.4', ls=':')
ax2.axvline(x=15, c='0.4', ls=':')
ax1.text(17.0, 0.5, r"$15.58$", fontsize=13)
ax2.axhline(y=0.01, c='0.4', ls=':')

ax1.set_xlim(1e-4, 100.)
pp.decor(ax1,
         ylim=(1e-3, 2.),
         ylabel=r"$CS(X_{\mathrm{c}})$",
         labels_kw={'size' : 15},
         ticks_kw={'labelsize' : 13}
)

#ax2.yaxis.set_ticks(np.linspace(-0.05, 0.25, num=7))
#ax2.grid(ls='--')
pp.decor(ax2,
         ylim=(1e-4, 0.5),
         ylabel=r"\textrm{Relative Error}",
         xlabel=r"$X_{\mathrm{c}}$",
         labels_kw={'size' : 15},
         ticks_kw={'labelsize' : 13}
)

pp.printer(fig, 'compareAll', savedir='/Users/jesus/lab/LaTesis/chambaFigures/', printEPS=True, printPDF=False, printPGF=False, tight=True)
