#!/usr/bin/env python -tt
from __future__ import (absolute_import, division, print_function)

import PypersPlots as pp
import extractor as extr
import colorcet as cc
import numpy as np
import time
import os
import matplotlib.colors as col
from matplotlib.ticker import AutoMinorLocator,LogLocator
import cubehelix as ch


dirin = "/Users/jesus/lab/cSPEV/tests/I1Error/"
tabName = dirin + "I1Error500x500-newRMA.h5"

chi, gamma = extr.hdf5Extract1D(tabName, ['chi1D', 'gamma1D'])
I1 = extr.hdf5Extract2D(tabName, 'I1table')

cbmin = 1e-6
cbmax = 1e0

thecmap = [
    ch.cmap(reverse=True, start=2.5, rot=4.5, gamma=0.8, sat=1.8, minLight=0.1, maxLight=0.8),
    'viridis',
    'gist_stern',
    'gist_earth',
    'inferno',
    'magma',
    'plasma',
    'gnuplot'
]

pp.latexify(txtwdth=369.0, ratio=4.0/3.0, lw=0.7)#, edgecol='w')
fig, ax = pp.initPlot()

ax.set_xscale('log')
ax.set_yscale('log')
ax.set_axis_off()
ax.margins(1.0)

I1_mask = np.ma.masked_where(I1 < 1e-6, I1)
M = ax.pcolormesh(chi,gamma,I1_mask[::-1,::-1],
                  cmap=thecmap[3],
                  norm=col.LogNorm(vmin=cbmin, vmax=cbmax),
                  rasterized=True,
                  shading='gouraud'
)

pp.decor(ax,
         ylim=(1e0, 1e3),
         xlim=(1e-3, 1e8),
)

d1 = '/Users/jesus/lab/LaTesis/Figures/'

fig.subplots_adjust(left=0.0,
                   right=1.0,
                   top=0.999,
                   bottom=0.01
)

pp.printer(fig, 'coverFig', savedir=d1, printEPS=True, printPDF=True, printPNG=True, printPGF=False, tight=False)
